//
//  BookNowViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "BookNowViewController.h"

@interface BookNowViewController ()

@end

@implementation BookNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    autoSelectDate = YES;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ss
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    if (![Customer currentCustomer]) {
        return ;
    }
    
    [dic setObject:TOKEN forKey:@"token"];
    [dic setObject:@"3" forKey:@"trip_type"];
    [dic setObject:@"" forKey:@"date"];
    [dic setObject:@"" forKey:@"pickup_time"];
    [dic setObject:@""   forKey:@"bags"];
    [dic setObject:@"" forKey:@"passengers"];
    [dic setObject:@"" forKey:@"notes"];
    
    [dic setObject:@"" forKey:@"car_seats"];
    NSDictionary  *destination = @{@"address":@"",@"lat":@"",@"lng":@""};
    [dic setObject:destination forKey:@"destination"];
    NSDictionary  *start = @{@"address":@"",@"lat":@"",@"lng":@""};
    [dic setObject:start forKey:@"destination"];
    
    Car * selectedCar = [[CarConnectionHandler sharedHandler] currentSelectCar];
    [dic setObject:selectedCar.carID forKey:@"car_type"];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
