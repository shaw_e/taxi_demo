//
//  BookLaterViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "BookLaterViewController.h"
#import "NSDate+DateFormatting.h"
@interface BookLaterViewController ()

@end

@implementation BookLaterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createDatePickerForTextField:dateTextField withDoneButtonSelecto:@selector(donePressed) andDatePickerSelector:@selector(dateUpdated:)];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createDatePickerForTextField:(UITextField *)textField withDoneButtonSelecto:(SEL) doneSelector andDatePickerSelector:(SEL)datePickerSelector
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:doneSelector];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, doneButton];
    textField.inputAccessoryView = toolbar;
    [toolbar sizeToFit];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.minuteInterval = 5;
    datePicker.minimumDate=[NSDate date];
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:datePickerSelector forControlEvents:UIControlEventValueChanged];
    textField.inputView = datePicker;
}
-(void)donePressed
{
    UIDatePicker * picker=(UIDatePicker *)dateTextField.inputView;
    [self dateUpdated:picker];
    [dateTextField resignFirstResponder];
    
}
- (void) dateUpdated:(UIDatePicker *)datePicker {
    dateTextField.text = [datePicker.date stringValue];
    rideDate = datePicker.date;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
