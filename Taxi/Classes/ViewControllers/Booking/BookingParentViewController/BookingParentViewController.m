//
//  BookingParentViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//
#import "CustomAnnotationView.h"
#import "BookingParentViewController.h"
#import "EditAdressViewController.h"
#import "UIViewController+NavigationBar.h"
#import "UIColor+Hex.h"
#import "NSDate+DateFormatting.h"
#import "BookingConnectionHandler.h"
#import "UIViewController+Loading.h"
#import "UIViewController+Alert.h"
#import "DTOBooking.h"
#import "BookingHolderViewController.h"
#import "NSString+CheckValue.h"



#define RATE_TAG @"rate"
#define BOOK_TAG @"book"
@interface BookingParentViewController ()<ServicesConnectionDelegate>

@end

@implementation BookingParentViewController
{
    BookingConnectionHandler * connectionHandler;
    NSString * fixedTitle;
    NSDate * date;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeBack];
    if (self.pickUp.address.length==0||[self.pickUp.address isEqualToString:@"loading Pick Up"]) {
        [self newPickUpLocation:self.pickUp.annotation];
    }else
    {
        [pickUpButton setTitle:[NSString stringWithFormat:@"Pick Up : %@",self.pickUp.address] forState:UIControlStateNormal];
    }
    if (self.dropOff.address.length==0||[self.dropOff.address isEqualToString:@"loading Drop Off"]) {
        [self newDropOffLocation:self.dropOff.annotation];

    }else
    {
        [dropOffButton setTitle:[NSString stringWithFormat:@"Drop Off : %@",self.dropOff.address] forState:UIControlStateNormal];

    }
    [mapView addAnnotation:self.pickUp.annotation];
    [mapView addAnnotation:self.dropOff.annotation];
    [self centerMap];
    connectionHandler =[BookingConnectionHandler new];
    [connectionHandler setDelegate:self];
    fixedTitle =actionButton.titleLabel.text;
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self rate];

}
-(IBAction)bookRide
{
    if (!autoSelectDate) {
        if (!rideDate) {
            [self showAlertDialogWithMessage:@"Select date and time"];
            return;
        }
    }
    [self book];
}
-(void)rate
{
    
    [connectionHandler setTagString:RATE_TAG];
    [connectionHandler costFromDictionary:[self bookingDictionary]];
    [actionButton setIsloading:YES];
}
-(void)book
{
    [self startLoading];
    [connectionHandler setTagString:BOOK_TAG];
    [connectionHandler bookRideFromDictionary:[self bookingDictionary]];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)autoSelectPickUp:(DTOlocation *)location
{
    [mapView removeAnnotation:self.pickUp.annotation];

    if (location.address.length==0||[location.address isEqualToString:@"loading Pick Up"]) {
        [self newPickUpLocation:location.annotation];
    }else
    {
        self.pickUp=location;
        [pickUpButton setTitle:[NSString stringWithFormat:@"Pick Up : %@",self.pickUp.address] forState:UIControlStateNormal];
    }
    [mapView addAnnotation:self.pickUp.annotation];
    [self centerMap];

}
-(void)autoSelectDropOff:(DTOlocation *)location
{
    [mapView removeAnnotation:self.dropOff.annotation];

    if (location.address.length==0||[location.address isEqualToString:@"loading Drop Off"]) {
        [self newDropOffLocation:location.annotation];
    }else
    {
        self.dropOff=location;

        [dropOffButton setTitle:[NSString stringWithFormat:@"Drop Off : %@",self.dropOff.address] forState:UIControlStateNormal];
    }
    [mapView addAnnotation:self.dropOff.annotation];
    [self centerMap];
    [self rate];


}
-(void)newDropOffLocation:(MKPointAnnotation *)ann
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation * location =[[CLLocation alloc]initWithLatitude:ann.coordinate.latitude longitude:ann.coordinate.longitude];
    __block MKPointAnnotation * coorindates = ann;
    self.dropOff.annotation=ann;
    [dropOffButton setTitle:@"loading Drop Off" forState:UIControlStateNormal];
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            //                    self.currentPlacemark = [placemarks lastObject];
            CLPlacemark *placemark=[placemarks lastObject];
            if (placemark.name.length >0) {
                if (self.dropOff.annotation==coorindates) {
//                    self.dropOff.value=placemark.name;
                    self.dropOff.address=placemark.name;//[NSString stringWithFormat:@"Drop off : %@",placemark.name];
                }
                
            }else
            {
                self.dropOff.address=@"Cannot detect pick up";
                
            }
            [dropOffButton setTitle:[NSString stringWithFormat:@"Drop Off : %@",self.dropOff.address] forState:UIControlStateNormal];
            
        }
    }];
    
}
-(void)newPickUpLocation:(MKPointAnnotation *)ann
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation * location =[[CLLocation alloc]initWithLatitude:ann.coordinate.latitude longitude:ann.coordinate.longitude];
    __block MKPointAnnotation * coorindates = ann;
    self.pickUp.annotation=ann;
    [pickUpButton setTitle:@"loading Pick Up" forState:UIControlStateNormal];
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            //                    self.currentPlacemark = [placemarks lastObject];
            CLPlacemark *placemark=[placemarks lastObject];
            
            if (placemark.name.length >0) {
                if (self.pickUp.annotation==coorindates) {
//                    self.pickUp.value=placemark.name;
                    self.pickUp.address=placemark.name;//[NSString stringWithFormat:@"Pick Up : %@",placemark.name];
                }
                
            }else
            {
                self.pickUp.address=@"Cannot detect pick up";
                
            }
            [pickUpButton setTitle:self.pickUp.address forState:UIControlStateNormal];

            
        }
    }];
    
    
}
#pragma mark- Map View + location
- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation)
        return nil;
    
    if (annotation == self.pickUp.annotation) {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pickUpAnn" color:[UIColor colorFromHexString:@"#a5c13e"]];
        
        return annView;
        
    }else
    {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"drop" color:[UIColor colorFromHexString:@"#c13e3e"]];
        
        return annView;
        
    }

    
}


-(void) centerMap
{
    [mapView showAnnotations:mapView.annotations animated:YES];
}
-(NSDictionary *)bookingDictionary
{
 
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    if (![Customer currentCustomer]) {
        return nil;
    }
    if (autoSelectDate) {
        if (!rideDate) {
            rideDate = [NSDate date];
            [dic setObject:[rideDate  desiredDateStringValue] forKey:@"date"];
            [dic setObject:[rideDate desiredTimeStringValue] forKey:@"pickup_time"];

        }
    }else
    {
        if (!rideDate) {
            [dic setObject:@"date" forKey:@"date"];
            [dic setObject:@"time" forKey:@"pickup_time"];
        }else
        {
            [dic setObject:[rideDate  desiredDateStringValue] forKey:@"date"];
            [dic setObject:[rideDate desiredTimeStringValue] forKey:@"pickup_time"];

        }
    }
  
    
    [dic setObject:TOKEN forKey:@"token"];
    [dic setObject:@"3" forKey:@"trip_type"];
    [dic setObject:@"1"   forKey:@"bags"];
    [dic setObject:@"1" forKey:@"passengers"];
    [dic setObject:@"" forKey:@"notes"];
    
    [dic setObject:@"" forKey:@"car_seats"];
    NSString * addressDes=([self.dropOff.address hasValue])?self.dropOff.address:@"";
    
    NSDictionary  *destination = @{@"address":addressDes,@"lat":[NSString stringWithFormat:@"%f",self.dropOff.annotation.coordinate.latitude],@"lng":[NSString stringWithFormat:@"%f",self.dropOff.annotation.coordinate.longitude]};
    [dic setObject:destination forKey:@"destination"];
    NSString * addressStart=([self.pickUp.address hasValue])?self.pickUp.address:@"";

    NSDictionary  *start = @{@"address":addressStart,@"lat":[NSString stringWithFormat:@"%f",self.pickUp.annotation.coordinate.latitude],@"lng":[NSString stringWithFormat:@"%f",self.pickUp.annotation.coordinate.longitude]};
    [dic setObject:start forKey:@"pickup"];

    Car * selectedCar = [[CarConnectionHandler sharedHandler] currentSelectCar];
    [dic setObject:selectedCar.carID forKey:@"car_type"];
    
    return dic;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"PickUp"]) {
        EditAdressViewController * vc=segue.destinationViewController;
        [vc setLocation:self.pickUp];
        [vc setRoot:self];
        [vc setTitle:@"Pick Up"];
        [vc setIsPickUp:YES];
    }else if ([segue.identifier isEqualToString:@"DropOff"])
    {
        EditAdressViewController * vc=segue.destinationViewController;
        [vc setLocation:self.dropOff];
        [vc setRoot:self];

        [vc setTitle:@"Drop Off"];

    }
}
#pragma mark-  Services delegate
-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"NoInternet");
    //    [self endLoadingWithErrorMessage:message];
    if ([tag isEqualToString:BOOK_TAG]) {
        [self showAlertDialogWithMessage:@"Check your internet connection"];
        [self endLoadingWithErrorMessage:@"Check your internet connection"];
    }else{
        [actionButton setIsloading:NO];
        
    }

    [connectionHandler finish];
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    if ([tag isEqualToString:BOOK_TAG]) {
        [self endLoadingWithErrorMessage:@"Server down"];
        [self showAlertDialogWithMessage:@"Try again later"];
    }else{
        [actionButton setIsloading:NO];

    }
    
    
    [connectionHandler finish];
}
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    if ([tag isEqualToString:BOOK_TAG]) {
        if ([[result objectForKey:@"responseCode"] isEqualToString:@"SUCCESS"]) {
            DTOBooking * booking = [[DTOBooking alloc]initWithDictionary:result[@"booking"]];
            BookingHolderViewController * vc =[BookingHolderViewController new];
            [vc setItem:booking];
            [self.navigationController pushViewController:vc animated:YES];
            [self endLoading];
        }else
        {
            [self dismissLoading];
        }

    }else{
        [actionButton setIsloading:NO];

        if ([[result objectForKey:@"msg"] isEqualToString:@"SUCCESS"]) {
            NSString *rate = [NSString stringWithFormat:@"%@ $%@",fixedTitle,  result[@"rates"]];
            UIFont *font = [UIFont systemFontOfSize:18];
            UIFont *bigFont = [UIFont boldSystemFontOfSize:20];
            
            NSMutableAttributedString * attrRate = [[NSMutableAttributedString alloc] initWithData:[rate dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            [attrRate addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, attrRate.length)];
            [attrRate addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, fixedTitle.length)];
            [attrRate addAttribute:NSFontAttributeName value:bigFont range:NSMakeRange(fixedTitle.length, attrRate.length - fixedTitle.length)];
            [actionButton setAttributedTitle:attrRate forState:UIControlStateNormal];
        }

        
    }
    
    [connectionHandler finish];
    
}

-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    if ([tag isEqualToString:BOOK_TAG]) {
        [self showAlertDialogWithMessage:@"Check your internet connection"];
        [self endLoadingWithErrorMessage:@"Check your internet connection"];
  
    }

    
    [connectionHandler finish];
}

@end
