//
//  BookingParentViewController.h
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DTOlocation.h"
#import "Constants.h"
#import "CarConnectionHandler.h"
#import "RoundCornerButton.h"

@interface BookingParentViewController : UIViewController
{
    __weak IBOutlet MKMapView * mapView;
    __weak IBOutlet UIButton * pickUpButton;
    __weak IBOutlet UIButton * dropOffButton;
    __weak IBOutlet RoundCornerButton * actionButton;
    //
    NSDate * rideDate;
    BOOL autoSelectDate;
}
@property (nonatomic,strong) DTOlocation * pickUp;
@property (nonatomic,strong) DTOlocation * dropOff;
-(void)autoSelectPickUp:(DTOlocation *)location;
-(void)autoSelectDropOff:(DTOlocation *)location;
-(IBAction)bookRide;
@end
