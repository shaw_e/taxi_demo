//
//  AddressViewController.h
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "EditAdressViewController.h"
@interface AddressViewController : UIViewController
@property (weak,nonatomic) MainViewController * root;
@property (weak,nonatomic) EditAdressViewController * editVC;

@end
