//
//  AddressViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "AddressViewController.h"
#import "UIViewController+NavigationBar.h"
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocompleteQuery.h>
#import <CoreLocation/CoreLocation.h>
#import "CLPlacemark+HNKAdditions.h"
#import "SWRevealViewController.h"
#import "DTOlocation.h"
#import "AddressConnectionHandler.h"
#import "UIViewController+Alert.h"
#import "UIViewController+Loading.h"
#import "UIView+Coordinates.h"



#define  ADD_TAG @"add"
#define  LIST_TAG @"list"
@interface AddressViewController ()<UITableViewDataSource,UITableViewDelegate,ServicesConnectionDelegate>
{
    __weak IBOutlet UITextField *searchTextField;
    __weak IBOutlet UITableView *autoCompleteTableView;
    __weak IBOutlet UITableView *cashedAddressTableView;
    HNKGooglePlacesAutocompleteQuery * searchQuery;
    Address *address;
    AddressConnectionHandler * connectionHandler;

}

@end

@implementation AddressViewController
{
    NSArray * arrayOfSearchResults;
    NSMutableArray * arrayOfCashedAddress;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMenu];
    [self customizeBack];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];

    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismissLoading];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!connectionHandler) {
        connectionHandler = [AddressConnectionHandler new];
        [connectionHandler setDelegate:self];
        [connectionHandler setTagString:LIST_TAG];
        [connectionHandler retrieveAddress];
        [self startLoading];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- Actions
- (IBAction)addAddress:(id)sender
{
    if (!address) {
        [self showAlertDialogWithMessage:@"Please enter a valid address"];
        return;
    }
    
    
    [connectionHandler setTagString:ADD_TAG];
    [connectionHandler addAddress:address];
    [self startLoading];
}

#pragma mark - Methods
-(void)showAutoComplete:(BOOL)willShow
{
    if (!willShow) {
        arrayOfSearchResults =nil;
        [autoCompleteTableView reloadData];
        [autoCompleteTableView setHidden:YES];
    }else
    {
        [autoCompleteTableView reloadData];
        [autoCompleteTableView setHidden:NO];
    }
}
-(void)selectAddressAtIndex:(int)index
{
    address=nil;
    HNKGooglePlacesAutocompletePlace *place = arrayOfSearchResults[index];
    [searchTextField setText:place.name];
    [self showAutoComplete:NO];
    [self startLoading];
    [searchTextField resignFirstResponder];
    [CLPlacemark hnk_placemarkFromGooglePlace:place
                                       apiKey:searchQuery.apiKey
                                   completion:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
                                       if (error) {
                                           [self endLoadingWithErrorMessage:@"Error"];
                                       } else if (placemark) {
                                           address = [[Address alloc] init];
                                           address.addressLat = [NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude];
                                           address.addressLong = [NSString stringWithFormat:@"%f",placemark.location.coordinate.longitude];
                                           address.addressName = place.name;
                                           [self endLoading];
                                           
                                       }
                                   }];

}
-(void)setLocationFromAddress
{
    DTOlocation * loc=[DTOlocation new];
    loc.address=address.addressName;
    loc.annotation=[MKPointAnnotation new];
    loc.annotation.coordinate=CLLocationCoordinate2DMake(address.addressLat.floatValue, address.addressLong.floatValue);
    if (self.root) {
        [self.root autoSelectLocation:loc];
    }else
    {
        [self.editVC autoSelectLocation:loc];
    }
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)createHeaderForCashed
{
    UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, cashedAddressTableView.width, 20)];
    [label setBackgroundColor:[UIColor grayColor]];
    [label setText:@"Previous"];
    [cashedAddressTableView setTableHeaderView:label];
}
#pragma mark- TextField + search
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField != searchTextField) {
        return YES;
    }
    
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    [self handleSearchForSearchString:proposedNewString];
    return YES;
}
- (void)handleSearchForSearchString:(NSString *)searchString
{
    
    if ([searchString isEqualToString:@""])
    {
        [self showAutoComplete:NO];
    }
    else
    {
        [self showAutoComplete:NO];
        
        searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
        
        [searchQuery fetchPlacesForSearchQuery:searchString
                                    completion:^(NSArray *places, NSError *error)
         {
             if (error)
             {
                 [self showAutoComplete:NO];
             }
             else
             {
                 arrayOfSearchResults = places;
                 [self showAutoComplete:YES];
             }
         }];
    }
}


#pragma mark- Table View
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==autoCompleteTableView) {
        return arrayOfSearchResults.count;
    }else
    {
        return arrayOfCashedAddress.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    if (tableView==autoCompleteTableView) {
        cell = [self cellForAutoCompleteAtIndex:indexPath];
    }else
    {
        cell = [self cellForCashedAtIndex:indexPath];
    }

    return cell;
}
-(UITableViewCell *)cellForAutoCompleteAtIndex:(NSIndexPath *)indexPath
{
    HNKGooglePlacesAutocompletePlace *place =arrayOfSearchResults[indexPath.row];

    UITableViewCell * cell =[autoCompleteTableView dequeueReusableCellWithIdentifier:@"cell2"];
    [cell.textLabel setText:place.name];
    return cell;
}
-(UITableViewCell *)cellForCashedAtIndex:(NSIndexPath *)indexPath
{
    UITableViewCell * cell =[cashedAddressTableView dequeueReusableCellWithIdentifier:@"cell"];
    Address * item =arrayOfCashedAddress[indexPath.row];
    [cell.textLabel setText:item.addressName];
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == autoCompleteTableView) {

        [self selectAddressAtIndex:(int)indexPath.row];
    }else
    {
        address =arrayOfCashedAddress[indexPath.row];
        [self setLocationFromAddress];
    }
}
#pragma mark- Services Delegate

-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"NoInternet");
    //    [self endLoadingWithErrorMessage:message];
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    [connectionHandler finish];
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"ServerDown");
    //    [self endLoadingWithErrorMessage:message];
    //    [self showAlertDialogWithMessage:message];
    [self showAlertDialogWithMessage:@"Try again later"];
    [self endLoadingWithErrorMessage:@"Server down"];
    
    
    [connectionHandler finish];
}
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    if ([tag isEqualToString:ADD_TAG]) {
        if (![[result objectForKey:@"msg"] isEqualToString:@"SUCCESS"]) {
            [self endLoadingWithErrorMessage:@"Address is not added, please try again."];

            [self showAlertDialogWithMessage:@"Address is not added, please try again." ];
        }else
        {
            [self endLoading];

            [self setLocationFromAddress];

        }

    }else
    {
        [self endLoading];

        arrayOfCashedAddress = [NSMutableArray new];
        NSArray *arr = [result objectForKey:@"addressesList"];
        for (int i = 0; i < [arr count]; i++) {
            Address *item = [[Address alloc] init];
            [item populateAddressWithDic:[arr objectAtIndex:i]];
            [arrayOfCashedAddress addObject:item];
        }
        [self createHeaderForCashed];
        [cashedAddressTableView reloadData];
        

    }
    [connectionHandler finish];
    
}

-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    
    [connectionHandler finish];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
