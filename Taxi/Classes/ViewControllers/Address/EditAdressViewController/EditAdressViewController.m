//
//  EditAdressViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "EditAdressViewController.h"
#import "CustomAnnotationView.h"
#import "AddressViewController.h"
#import "UIColor+Hex.h"
#import "UIViewController+NavigationBar.h"
@interface EditAdressViewController ()

@end

@implementation EditAdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeBack];
    if (self.location.address.length==0||[self.location.address isEqualToString:@"loading Pick Up"]) {
        [self newFromLocation:self.location.annotation];
    }else
    {
        [line1Label setText:[NSString stringWithFormat:@"%@ : %@",self.title,self.location.address]];
    }
    [mapView addAnnotation:self.location.annotation];
    [self centerMap];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongGesture:)];
    lpgr.minimumPressDuration = 1.0;  //user must press for 2 seconds
    [mapView addGestureRecognizer:lpgr];
    UITapGestureRecognizer * tgr =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [linesHolder addGestureRecognizer:tgr];


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleTapGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self performSegueWithIdentifier:@"Address" sender:nil];
    
}


-(void)autoSelectLocation:(DTOlocation *)newLoc
{
        [mapView removeAnnotation:self.location.annotation];
        self.location=newLoc;
        self.location.address=[NSString stringWithFormat:@"%@",self.location.address];

    [line1Label setText:[NSString stringWithFormat:@"%@ : %@",self.title,self.location.address]];
        [mapView addAnnotation:self.location.annotation];
    [self centerMap];
}

- (IBAction)Confirm:(id)sender {
    if (self.isPickUp) {
        [self.root autoSelectPickUp:self.location];
    }else
    {
        [self.root autoSelectDropOff:self.location];

    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)newFromLocation:(MKPointAnnotation *)ann
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation * location =[[CLLocation alloc]initWithLatitude:ann.coordinate.latitude longitude:ann.coordinate.longitude];
    __block MKPointAnnotation * coorindates = ann;
    self.location.annotation=ann;
//    self.location.address =@"loading Drop Off";
    [line1Label setText:@"Loading"];
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            //                    self.currentPlacemark = [placemarks lastObject];
            CLPlacemark *placemark=[placemarks lastObject];
            if (placemark.name.length >0) {
                if (self.location.annotation==coorindates) {
                    self.location.address=[NSString stringWithFormat:@"%@",placemark.name];
                }
                
            }else
            {
                [line1Label setText:@"Cannot detect pick up"];

            }
            [line1Label setText:[NSString stringWithFormat:@"%@ : %@",self.title,self.location.address]];
        }
    }];
    
}
- (void)handleLongGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    [mapView removeAnnotations:mapView.annotations];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [mapView convertPoint:touchPoint toCoordinateFromView:mapView];
    
    MKPointAnnotation *pa = [[MKPointAnnotation alloc] init];
    pa.coordinate = touchMapCoordinate;
    pa.title = @" ";
    [mapView addAnnotation:pa];
        //        pickUpAnn=pa;
        [self newFromLocation:pa];
        
    
    
}

#pragma mark- Map View + location
- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation)
        return nil;
    
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pickUpAnn" color:[UIColor colorFromHexString:@"#a5c13e"]];
        
        return annView;
    
    
    

}
-(void) centerMap
{
    [mapView showAnnotations:mapView.annotations animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Address"]) {
        AddressViewController * vc = segue.destinationViewController;
        [vc setEditVC:self];
    }
   
}

@end
