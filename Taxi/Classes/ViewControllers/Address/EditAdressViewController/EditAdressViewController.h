//
//  EditAdressViewController.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTOlocation.h"
#import "BookingParentViewController.h"
@interface EditAdressViewController : UIViewController
{
    __weak IBOutlet MKMapView * mapView;
    __weak IBOutlet UILabel *line1Label;
    __weak IBOutlet UILabel *line2Label;
    __weak IBOutlet UIView *linesHolder;

}
@property (nonatomic,copy) DTOlocation * location;
@property (weak,nonatomic) BookingParentViewController * root;
@property(nonatomic)BOOL isPickUp;
-(void)autoSelectLocation:(DTOlocation *)newLoc;
@end
