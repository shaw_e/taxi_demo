//
//  MainViewController.h
//  Taxi
//
//  Created by ahmed shawky on 4/3/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DTOlocation;
@interface MainViewController : UIViewController
-(void)autoSelectLocation:(DTOlocation *)location;

@end
