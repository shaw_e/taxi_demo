//
//  MainViewController.m
//  Taxi
//
//  Created by ahmed shawky on 4/3/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//
//BookNow
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "iCarousel.h"
#import "UIView+Coordinates.h"
#import "AsyncImageView.h"
#import "AddressViewController.h"
#import "UIViewController+NavigationBar.h"
#import "CustomAnnotationView.h"
#import "DTOlocation.h"
#import "BookingParentViewController.h"
#import "Constants.h"
#import "CarConnectionHandler.h"
#import "UIViewController+Loading.h"
#import "UIViewController+Alert.h"
#import "NSString+CheckValue.h"

@interface MainViewController ()<iCarouselDataSource,iCarouselDelegate,MKMapViewDelegate,CLLocationManagerDelegate,ServicesConnectionDelegate>
{
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet iCarousel * carsCarousel;
    __weak IBOutlet UILabel *line1Label;
    __weak IBOutlet UILabel *line2Label;
    __weak IBOutlet UIButton *bookNowButton;
    __weak IBOutlet UIButton *bookLaterButton;
    __weak IBOutlet UIView *linesHolder;
    __weak IBOutlet UISegmentedControl *segmented;
    __weak IBOutlet NSLayoutConstraint *carsHolderHeight;
}
@end

@implementation MainViewController
{
    DTOlocation * pickUp,*dropOff;
    CLLocationManager * locationManager;
    __weak CarConnectionHandler * connectionHandler;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMenu];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handleLongGesture:)];
            lpgr.minimumPressDuration = 2.0;  //user must press for 2 seconds
            [mapView addGestureRecognizer:lpgr];
            UITapGestureRecognizer * tgr =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            [linesHolder addGestureRecognizer:tgr];
            [carsCarousel setDelegate:self];
            [carsCarousel setDataSource:self];
            [carsCarousel setType:iCarouselTypeLinear];
    dropOff = [DTOlocation new];
//    dropOff.address =@"Choose Drop Off";
    pickUp=[DTOlocation new];
//    pickUp.address =@"Choose Pick Up";
    [line1Label setText:@"Choose Pick Up"];
    [self createLocationManager];
    connectionHandler = [CarConnectionHandler sharedHandler];


    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![Customer currentCustomer]) {
        [self performSegueWithIdentifier:@"LogIn" sender:nil];
    }else
    {
        if (![connectionHandler arrayOfCars]) {
            [connectionHandler setDelegate:self];
            [connectionHandler retrieveCars];
            [self startLoading];
            [bookLaterButton setEnabled:NO];
            [bookNowButton setEnabled:NO];
        }else
        {
            [carsCarousel reloadData];
        }
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismissLoading];
}

//-(void)viewDidAppear:(BOOL)animated
//{
//    if (!carsCarousel) {
//        iCarousel * car=[[iCarousel alloc]initWithFrame:CGRectMake(mapView.x, mapView.y, mapView.width, 100)];
//        [car setDelegate:self];
//        [car setDataSource:self];
//        [car setType:iCarouselTypeCustom];
//        [self.view addSubview:car];
//        carsCarousel = car;
////        [carsCarousel scrollToItemAtIndex:2 animated:YES];
//        [car setBackgroundColor:[UIColor lightGrayColor]];
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongGesture:)];
//        lpgr.minimumPressDuration = 2.0;  //user must press for 2 seconds
//        [mapView addGestureRecognizer:lpgr];
//        UITapGestureRecognizer * tgr =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
//        [linesHolder addGestureRecognizer:tgr];
//
//    }
//}
#pragma mark- Gesture
- (void)handleLongGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    MKPointAnnotation * old;
    if (segmented.selectedSegmentIndex ==0) {
        
        old= pickUp.annotation;
    }else
    {
        old = dropOff.annotation;
    }
    [mapView removeAnnotation:old];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [mapView convertPoint:touchPoint toCoordinateFromView:mapView];
    
    MKPointAnnotation *pa = [[MKPointAnnotation alloc] init];
    pa.coordinate = touchMapCoordinate;
    pa.title = @" ";
    [mapView addAnnotation:pa];
    if (segmented.selectedSegmentIndex ==0) {
//        pickUpAnn=pa;
        [self newPickUpLocation:pa];
    
    }else
    {
        [self newDropOffLocation:pa];
    }


}
- (void)handleTapGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self performSegueWithIdentifier:@"Address" sender:nil];
    
}

#pragma mark- select location
-(void)autoSelectLocation:(DTOlocation *)location
{
    if (segmented.selectedSegmentIndex==0) {
        [mapView removeAnnotation:pickUp.annotation];
        pickUp=location;
        pickUp.address=pickUp.address;//[NSString stringWithFormat:@"Pick Up : %@",pickUp.address];

        [line1Label setText:[NSString stringWithFormat:@"Pick Up : %@",pickUp.address]];
        [mapView addAnnotation:pickUp.annotation];
    
    }else
    {
        [mapView removeAnnotation:dropOff.annotation];
        dropOff=location;
        dropOff.address=dropOff.address;//[NSString stringWithFormat:@"Drop off : %@",dropOff.address];
        [line1Label setText:[NSString stringWithFormat:@"Drop Off : %@",dropOff.address]];
        [mapView addAnnotation:dropOff.annotation];
    }
    [self centerMap];
}
-(void)newDropOffLocation:(MKPointAnnotation *)ann
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
     CLLocation * location =[[CLLocation alloc]initWithLatitude:ann.coordinate.latitude longitude:ann.coordinate.longitude];
    __block MKPointAnnotation * coorindates = ann;
    dropOff.annotation=ann;
    [line1Label setText:@"loading Drop Off"];
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            //                    self.currentPlacemark = [placemarks lastObject];
            CLPlacemark *placemark=[placemarks lastObject];
            if (placemark.name >0) {
                if (dropOff.annotation==coorindates) {
//                    dropOff.value = placemark.name;
                    dropOff.address=placemark.name;//[NSString stringWithFormat:@"Drop off : %@",placemark.name];
                }

            }else
            {
                dropOff.address=@"Cannot detect pick up";

            }
                if (segmented.selectedSegmentIndex==1) {
                    [line1Label setText:[NSString stringWithFormat:@"Drop Off : %@",dropOff.address]];
                }

            
        }
    }];

}
-(void)newPickUpLocation:(MKPointAnnotation *)ann
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation * location =[[CLLocation alloc]initWithLatitude:ann.coordinate.latitude longitude:ann.coordinate.longitude];
    __block MKPointAnnotation * coorindates = ann;
    pickUp.annotation=ann;
//    pickUp.address =@"loading Pick Up";
    [line1Label setText:@"loading Pick Up"];
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            //                    self.currentPlacemark = [placemarks lastObject];
            CLPlacemark *placemark=[placemarks lastObject];
        
            
            if (placemark.name >0) {
                if (pickUp.annotation==coorindates) {
//                    pickUp.value = placemark.name;

                    pickUp.address=placemark.name;
                }
                
            }else
            {
                pickUp.address=@"Cannot detect pick up";
                
            }
            if (segmented.selectedSegmentIndex==0) {
                [line1Label setText:[NSString stringWithFormat:@"Pick Up : %@",pickUp.address]];
            }

        }
    }];
    

}
#pragma mark- Actions
- (IBAction)segmentedChanged:(UISegmentedControl *)sender {
    if (segmented.selectedSegmentIndex==0) {
        if ([pickUp.address hasValue]) {
            [line1Label setText:[NSString stringWithFormat:@"Pick Up : %@",pickUp.address]];
        }else
        {
            if (pickUp.annotation) {
                [line1Label setText:@"Loading Pick Up"];
            }else
            {
                [line1Label setText:@"Choose Pick Up"];
 
            }

        }
    }else
        
    {
        if ([dropOff.address hasValue]) {
            [line1Label setText:[NSString stringWithFormat:@"Drop Off : %@",dropOff.address]];
        }else
        {
            if (dropOff.annotation) {
                [line1Label setText:@"Loading Drop Off"];
            }else
            {
                [line1Label setText:@"Choose Drop Off"];
                
            }
            
        }

    }
}
- (IBAction)bookNow:(id)sender {
    if (!pickUp.annotation) {
        [self showAlertDialogWithMessage:@"Select Pick Up"];
        return;
    }
    if (!dropOff.annotation) {
        [self showAlertDialogWithMessage:@"Select Dropp Off"];
        return;
    }
    [self performSegueWithIdentifier:@"BookNow" sender:nil];
}
- (IBAction)Booklater:(id)sender {
    if (!pickUp.annotation) {
        [self showAlertDialogWithMessage:@"Select Pick Up"];
        return;
    }
    if (!dropOff.annotation) {
        [self showAlertDialogWithMessage:@"Select Dropp Off"];
        return;
    }

    [self performSegueWithIdentifier:@"Later" sender:nil];

}

#pragma mark- Carousel
-(UIView *)initialView
{
    int totalHieght = carsCarousel.height;
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 75, totalHieght)];
    AsyncImageView * imageView=[[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 45, totalHieght-21)];
    [imageView setTag:111];
    [view addSubview:imageView];
    UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(5, totalHieght-17, 65, 17)];
    [label setFont:[UIFont systemFontOfSize:10]];
    [label setTag:222];
    label.adjustsFontSizeToFitWidth = YES;
    [label setMinimumScaleFactor:0.7];
    [label setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:label];
    UIView * line =[[UIView alloc ]initWithFrame:CGRectMake(5,totalHieght-1,65,1)];
    [line setTag:333];
    [view addSubview:line];
    return view;
    
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return connectionHandler.arrayOfCars.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (view == nil) {
        view = [self initialView];
    }
    AsyncImageView * imageView=(AsyncImageView *)[view viewWithTag:111];
    UILabel * label=(UILabel *)[view viewWithTag:222];
    UIView * line = [view viewWithTag:333];
    [label setText:@"car"];
    if (index == connectionHandler.selectedCar) {
        [line setBackgroundColor:THEME_COLOR];
        [label setTextColor:THEME_COLOR];
    }else
    {
        [line setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor blackColor]];

    }
    [imageView cancel];
    //
    Car * item = connectionHandler.arrayOfCars[index];
    [label setText:item.carType];
    [imageView setImageURLString:item.carImage];
    
    
    return view;
    
}
- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{

    connectionHandler.selectedCar = (int)index;
    [carsCarousel reloadData];
}
-(CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    CGFloat distance = 100.0f; //number of pixels to move the items away from camera
    CGFloat spacing = 0.0; //extra spacing for center item
    
    CGFloat clampedOffset = MIN(1.0, MAX(-1.0, offset));
    CGFloat z = - fabs(clampedOffset) * distance;
    offset += clampedOffset * spacing;
    return CATransform3DTranslate(transform, offset * carsCarousel.itemWidth, 0.0f, z);
}
- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.0f;
        }
        case iCarouselOptionFadeMax:
        {
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        {
            return  360;
        }
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        {
            //            return 10;
        }
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark- Map View + location
- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation)
        return nil;

    if (annotation == pickUp.annotation) {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pickUpAnn" color:[UIColor colorFromHexString:@"#a5c13e"]];
        
        return annView;

    }else
    {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"drop" color:[UIColor colorFromHexString:@"#c13e3e"]];
        
        return annView;

    }
    
}
-(void) centerMap
{
    [mapView showAnnotations:mapView.annotations animated:YES];
//    NSArray * arrayOfAnnotaions=[mapView.annotations copy];
//    MKCoordinateRegion region;
//    
//    CLLocationDegrees maxLat = -90;
//    CLLocationDegrees maxLon = -180;
//    CLLocationDegrees minLat = 90;
//    CLLocationDegrees minLon = 180;
//    for(int idx = 0; idx < arrayOfAnnotaions.count; idx++)
//    {
//        MKPointAnnotation * currentLocation = [arrayOfAnnotaions objectAtIndex:idx];
//        if(currentLocation.coordinate.latitude > maxLat)
//            maxLat = currentLocation.coordinate.latitude;
//        if(currentLocation.coordinate.latitude < minLat)
//            minLat = currentLocation.coordinate.latitude;
//        if(currentLocation.coordinate.longitude > maxLon)
//            maxLon = currentLocation.coordinate.longitude;
//        if(currentLocation.coordinate.longitude < minLon)
//            minLon = currentLocation.coordinate.longitude;
//    }
//    region.center.latitude     = (maxLat + minLat) / 2;
//    region.center.longitude    = (maxLon + minLon) / 2;
//    region.span.latitudeDelta  = maxLat - minLat+0.2;
//    region.span.longitudeDelta = maxLon - minLon+0.2;
//    if (region.center.latitude!=0) {
//        [mapView setRegion:region animated:YES];
//    }
}
-(void)createLocationManager
{
    if (!locationManager) {
        locationManager=[[CLLocationManager alloc]init];
        [locationManager setDelegate:self];
        locationManager.desiredAccuracy=kCLLocationAccuracyBest;
        locationManager.distanceFilter=kCLDistanceFilterNone;
    }
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ( [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted) {
            //            [self showAlertDialogWithMessage:Localized(@"RestrictAppGPS")];
            //            return;
        }
        
        if ( [CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
            //            [self showAlertDialogWithMessage:Localized(@"EnableAppGPS")];
            //            return;
        }
        
        [locationManager requestWhenInUseAuthorization];
    }
    if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
        //        [ZAActivityBar showWithStatus:Localized(@"LoadingLocation")];
    }else
    {
        
        //        [self showAlertDialogWithMessage:Localized(@"EnableGPS")];
    }
}
#pragma mark- Location Delegate
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    //    NSString * errorMessage;
    
    //    if (error.code==kCLErrorDenied) {
    //        errorMessage=Localized(@"EnableAppGPS");
    //    }else
    //    {
    //        errorMessage=Localized(@"LocationFailed");
    //    }
    //    [self showAlertDialogWithMessage:errorMessage];
    //    [ZAActivityBar showErrorWithStatus:errorMessage];
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [locationManager stopUpdatingLocation];
    CLLocation * location=[locations lastObject];
    if (!pickUp.annotation) {
        MKPointAnnotation * point =[MKPointAnnotation new];
        point.coordinate=location.coordinate;
        [mapView addAnnotation:point];
        [self newPickUpLocation:point];
        [self centerMap];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Address"]) {
        AddressViewController * vc = segue.destinationViewController;
        [vc setRoot:self];
    }
    if ([segue.identifier isEqualToString:@"BookNow"]||[segue.identifier isEqualToString:@"Later"]) {
        BookingParentViewController * bookVC=segue.destinationViewController;
        [bookVC setPickUp:pickUp];
        [bookVC setDropOff:dropOff];
    }
    
}
#pragma mark- Services Delegate

-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"NoInternet");
    //    [self endLoadingWithErrorMessage:message];
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    [connectionHandler finish];
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"ServerDown");
    //    [self endLoadingWithErrorMessage:message];
    //    [self showAlertDialogWithMessage:message];
    [self showAlertDialogWithMessage:@"Try again later"];
    [self endLoadingWithErrorMessage:@"Server down"];
    
    
    [connectionHandler finish];
}
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    [self endLoading];
    NSMutableArray * array =[NSMutableArray new];
    NSArray *arr = [result objectForKey:@"itemsList"];
    for (int i = 0; i < [arr count]; i++) {
        Car *car = [[Car alloc] init];
        [car initCarWithDic:[arr objectAtIndex:i]];
//        if (i == 0) {
//            car.isChecked = YES;
//        }
        [array addObject:car];
    }
    [bookLaterButton setEnabled:YES];
    [bookNowButton setEnabled:YES];

    
    [[CarConnectionHandler sharedHandler] setArrayOfCars:array];
    [carsCarousel reloadData];

//    if (![[result objectForKey:@"msg"] isEqualToString:@"CUSTOMERREGISTERED"]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"This email address is already registered." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        
//    }else
//    {
//        Customer *customer = [[Customer alloc] init];
//        [customer populateCustomerWithDictionary:postedParams];
//        [customer setAsCurrent];
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    }
    [connectionHandler finish];
    
}

-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    
    [connectionHandler finish];
}

@end

