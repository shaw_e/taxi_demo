//
//  LoginViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "LoginViewController.h"
#import "UserConnectionHandler.h"
#import "UIViewController+Loading.h"
#import "UIViewController+Alert.h"
#import "Customer.h"


@interface LoginViewController ()<ServicesConnectionDelegate>

@end

@implementation LoginViewController
{
    UserConnectionHandler * connectionHandler;

}
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([Customer currentCustomer]) {
        [emailTF setText:[[Customer currentCustomer] customerEmail]];
        [passwordTF setText:[[Customer currentCustomer] customerPassword]];
        [self login:nil];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismissLoading];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == emailTF)
    {
        [passwordTF becomeFirstResponder];
    }
    else
    {
        [passwordTF resignFirstResponder];
        [self login:nil];
    }
    return true;
}
- (IBAction)login:(id)sender
{
    if ([emailTF.text isEqualToString:@""] ||
        [passwordTF.text isEqualToString:@""]) {
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please complete your information." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
        [self showAlertDialogWithMessage:@"Please complete your information."];
        
        return;
    }
    connectionHandler=[UserConnectionHandler new];
    connectionHandler .delegate= self;
    [connectionHandler logInFromDixtionary:@{@"email":emailTF.text,@"password":passwordTF.text}];
    [self startLoading];
    
    ///
    
}
#pragma mark- Services Delegate

-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag
{
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    [connectionHandler finish];
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    [self showAlertDialogWithMessage:@"Try again later"];
    [self endLoadingWithErrorMessage:@"Server down"];


    [connectionHandler finish];
}
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    [self dismissLoading];
    if (![[result objectForKey:@"msg"] isEqualToString:@"SUCCESS"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Username or password is incorrect." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }else
    {
        Customer *customer = [[Customer alloc] init];
        [customer populateCustomerWithDictionary:[result objectForKey:@"customer"]];
        customer.token = [result objectForKey:@"token"];
        customer.customerPassword = passwordTF.text;
        [customer setAsCurrent];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    }
    [connectionHandler finish];

}

-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    //    if (error.code==NSURLErrorCancelled) {
    //        [self dismissLoading];
    //        return;
    //    }
    //    NSString * message=error.localizedDescription;
    //    [self endLoadingWithErrorMessage:message];
    //    [self showAlertDialogWithMessage:message];
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];

    [connectionHandler finish];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
