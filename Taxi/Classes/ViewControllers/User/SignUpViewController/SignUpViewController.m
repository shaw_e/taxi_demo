//
//  SignUpViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "SignUpViewController.h"
#import "UIViewController+NavigationBar.h"
#import "UserConnectionHandler.h"
#import "UIViewController+Loading.h"
#import "UIViewController+Alert.h"
#import "Customer.h"

@interface SignUpViewController ()<ServicesConnectionDelegate>

@end

@implementation SignUpViewController
{
    BOOL isFirstLoad;
    UserConnectionHandler * connectionHandler;
    NSDictionary * postedParams;

}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismissLoading];
}

- (void)viewDidLoad {
    [self customizeBack];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)registerUser:(id)sender
{
    if ([emailTF.text isEqualToString:@""] ||
        [passwordTF.text isEqualToString:@""] ||
        [phoneTF.text isEqualToString:@""] ||
        [firstNameTF.text isEqualToString:@""] ||
        [lastNameTF.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please complete your information." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    connectionHandler=[UserConnectionHandler new];
    connectionHandler .delegate= self;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:firstNameTF.text forKey:@"first_name"];
    [params setObject:lastNameTF.text forKey:@"last_name"];
    [params setObject:emailTF.text forKey:@"email"];
    [params setObject:passwordTF.text forKey:@"password"];
    [params setObject:phoneTF.text forKey:@"phone"];
    [params setObject:@"" forKey:@"address"];
    [params setObject:passwordTF.text forKey:@"password_confirmation"];
    postedParams =params;
    [connectionHandler registerFromDictionary:params];
    [self startLoading];

    //
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == firstNameTF)
    {
        [lastNameTF becomeFirstResponder];
    }
    else if(textField == lastNameTF)
    {
        [phoneTF becomeFirstResponder];
    }
    else if(textField == phoneTF)
    {
        [emailTF becomeFirstResponder];
    }
    else if(textField == emailTF)
    {
        [passwordTF becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self registerUser:nil];
    }
    
    return true;
}

#pragma mark- Services Delegate

-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"NoInternet");
    //    [self endLoadingWithErrorMessage:message];
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    [connectionHandler finish];
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    //    NSString * message=stringForKey(@"ServerDown");
    //    [self endLoadingWithErrorMessage:message];
    //    [self showAlertDialogWithMessage:message];
    [self showAlertDialogWithMessage:@"Try again later"];
    [self endLoadingWithErrorMessage:@"Server down"];
    
    
    [connectionHandler finish];
}
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    if (![[result objectForKey:@"msg"] isEqualToString:@"CUSTOMERREGISTERED"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"This email address is already registered." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [self dismissLoading];

    }else
    {
        [self endLoading];

        Customer *customer = [[Customer alloc] init];
        [customer populateCustomerWithDictionary:postedParams];
        [self.navigationController popViewControllerAnimated:YES];

    }
    [connectionHandler finish];

}

-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    [self showAlertDialogWithMessage:@"Check your internet connection"];
    [self endLoadingWithErrorMessage:@"Check your internet connection"];
    
    [connectionHandler finish];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
