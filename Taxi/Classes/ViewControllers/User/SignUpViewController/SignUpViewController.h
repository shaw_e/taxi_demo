//
//  SignUpViewController.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
{
   __weak IBOutlet UITextField *emailTF;
   __weak IBOutlet UITextField *passwordTF;
   __weak IBOutlet UITextField *phoneTF;
   __weak IBOutlet UITextField *firstNameTF;
   __weak IBOutlet UITextField *lastNameTF;
   __weak IBOutlet UIButton *regBtn;;

}
@end
