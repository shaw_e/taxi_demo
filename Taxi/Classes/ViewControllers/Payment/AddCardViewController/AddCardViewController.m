//
//  AddCardViewController.m
//  Taxi
//
//  Created by Ahmed shawky on 4/7/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "AddCardViewController.h"
#import "UIViewController+NavigationBar.h"
#import "PaymentHandler.h"
#import "DTOPayment.h"
@interface AddCardViewController ()
{
    __weak IBOutlet UITextField *cardNumberTextField;
    __weak IBOutlet UITextField *nameTextField;
    __weak IBOutlet UITextField *cvvTextField;
    __weak IBOutlet UITextField *codeTextField;
}
@end

@implementation AddCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeBack];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)registerUser:(id)sender
{
    if ([cardNumberTextField.text isEqualToString:@""] ||
        [nameTextField.text isEqualToString:@""] ||
        [cvvTextField.text isEqualToString:@""] ||
        [codeTextField.text isEqualToString:@""] ) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please complete your information." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    PaymentHandler * handler=[PaymentHandler shared];
    DTOPayment * item =[DTOPayment new];
    [item setCard:cardNumberTextField.text];
    [item setCcv:cvvTextField.text];
    [item setZipCode:codeTextField.text];
    [item setName:nameTextField.text];
    [handler.arrayOfCards addObject:item];
    [handler save];
    [self.navigationController popViewControllerAnimated:YES];
    
    //
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == cardNumberTextField)
    {
        [nameTextField becomeFirstResponder];
    }
    else if(textField == nameTextField)
    {
        [cvvTextField becomeFirstResponder];
    }
    else if(textField == cvvTextField   )
    {
        [codeTextField becomeFirstResponder];
    }
    
    else
    {
        [textField resignFirstResponder];
        [self registerUser:nil];
    }
    
    return true;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
