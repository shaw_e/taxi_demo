//
//  BookingHolderViewController.h
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTOBooking.h"
@interface BookingHolderViewController : UIViewController
@property(nonatomic,strong)DTOBooking * item;
-(void)refreshBooking;
@end
