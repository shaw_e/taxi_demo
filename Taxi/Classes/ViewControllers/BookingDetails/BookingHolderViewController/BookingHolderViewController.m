//
//  BookingHolderViewController.m
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "BookingHolderViewController.h"
#import "TrackingAfterPickUpViewController.h"
#import "TrackingBeforePickUpViewController.h"
#import "DetialsViewController.h"
#import "Taxi-swift.h"
#import "BookingConnectionHandler.h"
#import "UIViewController+NavigationBar.h"
#import "DTOlocation.h"
#import "UIViewController+Loading.h"
#import "NSString+CheckValue.h"

@interface BookingHolderViewController ()
{
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UISegmentedControl *segmented;
    UIPageViewController * pageViewController;
    DetialsViewController * detailsVC;
    TrackingParentViewController * summaryVC;
    SocketIOClient* socket;
    BOOL rideIsStarted,isConnected;
    NSString * rideStatus;
//    int trialIndex;
}

@end

@implementation BookingHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeBack];
    
    
    [titleLabel setText:self.item.booking_details.details_confirmation_number];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!pageViewController) {
        detailsVC =[DetialsViewController new];
        [detailsVC setItem:self.item];
        summaryVC=[TrackingBeforePickUpViewController new];
        [summaryVC setBooking:self.item];
        rideIsStarted = NO;
        //
        pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//        pageViewController.dataSource = self;
        
        UIViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        // Change the size of page view controller
        pageViewController.view.frame = CGRectMake(0, segmented.frame.size.height+segmented.frame.origin.y+8, self.view.frame.size.width, self.view.frame.size.height - (segmented.frame.size.height+segmented.frame.origin.y+8));
        
        [self addChildViewController:pageViewController];
        [self.view addSubview:pageViewController.view];
        [pageViewController didMoveToParentViewController:self];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self initSocket];
 
    });
}
-(void)refreshBooking
{
    [detailsVC setItem:self.item];
    [summaryVC setBooking:self.item];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Socket
-(void)initSocket
{
    NSURL* url = [[NSURL alloc] initWithString:@"http://104.131.35.18:3322"];
    socket = [[SocketIOClient alloc] initWithSocketURL:url options:@{@"log": @NO, @"forcePolling": @YES}];
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary * dictionary=@{@"customer_id":self.item.booking_customer.customerID};
        [socket emit:@"join" withItems:@[dictionary]];
        isConnected= YES;
        [self addHandlers];
//
//        ///
//        if (!rideIsStarted) {
//            [self startRide];
//        }

    }];
    [socket connect];

}
-(void)addHandlers
{
    [socket onAny:^(SocketAnyEvent * _Nonnull handler) {
        NSLog(@"%@",handler.event);
        NSLog(@"%@",handler.items);
    }];
    [socket on:@"NewDriverLocation" callback:^(NSArray* data, SocketAckEmitter* ack)  {
        [self driverLocation:data];
    }];
    [socket on:@"RideStatus" callback:^(NSArray* data, SocketAckEmitter* ack)  {
      
        [self rideStatus:data];
        
        
    }];

    [socket on:@"reconnect" callback:^(NSArray* data, SocketAckEmitter* ack)  {
        
    }];
    [socket on:@"Error" callback:^(NSArray* data, SocketAckEmitter* ack)  {
        [self socketError:data];
    }];
    [socket on:@"error" callback:^(NSArray* data, SocketAckEmitter* ack)  {
        [self socketError:data];
    }];
    
    [socket on:@"reconnectAttempt" callback:^(NSArray* data, SocketAckEmitter* ack)  {
        
    }];

}
#pragma mark socket+UI

-(void)didPickUp
{
    [self endLoadingWithSuccessMessage:@"Welcome on board"];
    summaryVC=[TrackingAfterPickUpViewController new];
    
    [summaryVC setBooking:self.item];
    rideIsStarted = YES;
    if (segmented.selectedSegmentIndex == 0) {
        UIViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}
-(void)rideStatus:(NSArray *)array
{
    NSDictionary * dic =array[0];
    NSString * bookingId=[NSString stringWithFormat:@"%@",dic[@"booking_id"]];
//    NSString * customer_id=[NSString stringWithFormat:@"%@",dic[@"requestID"]];
    NSString * status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    if (bookingId.intValue == self.item.booking_details.details_bookindID.intValue) {
        if ([status isEqualToString:@"CallOut"]) {
            [self driverStart];
            
        }else if ([status isEqualToString:@"WFP"]) {
            [self driverWaiting];
        
        }else if ([status isEqualToString:@"POB"]) {
            [self didPickUp];
            
        }else if ([status isEqualToString:@"Completed"]) {
            [self rideCompleted];
        }
    }
    
 
}
-(void)driverStart
{
    [self endLoadingWithSuccessMessage:@"ON THE WAY"];
}
-(void)driverWaiting
{
    [self endLoadingWithSuccessMessage:@"Driver waiting"];

}

-(void)rideCompleted
{
    [summaryVC removeDriverLocation];
    [self endLoadingWithSuccessMessage:@"Ride Completed"];
    [segmented setSelectedSegmentIndex:1];
    UIViewController *startingViewController = [self viewControllerAtIndex:1];
    NSArray *viewControllers = @[startingViewController];
    [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];

}
-(void)driverLocation:(NSArray *)array
{
    NSDictionary * dictionary = array[0];
    NSString * bookingId=[NSString stringWithFormat:@"%@",dictionary[@"booking_id"]];
    if (bookingId.intValue == self.item.booking_details.details_bookindID.intValue) {

    NSDictionary * driverDictionary =[dictionary objectForKey:@"driverDetails"];
    DTODriver * driver =[[DTODriver alloc]initWithDictionary:driverDictionary];
//    if (![self.item.booking_driver.driver_fullName hasValue]) {
        self.item.booking_driver=driver;
        //FIXME: send driver to ui
        [summaryVC updateDriverHeaderWithCarNumber:[NSString stringWithFormat:@"Car#: %@",driverDictionary[@"car_id"]]];
//    }
    DTOlocation * driverLocation =[DTOlocation new];
    [driverLocation setAddress:dictionary[@"duration"]];
    driverLocation.annotation=[[MKPointAnnotation alloc]init];
//    if (trialIndex > 5) {
//        trialIndex =0;
//    }
//    trialIndex ++;
//    switch (trialIndex) {
//        case 1:
//            [driverLocation.annotation setCoordinate:CLLocationCoordinate2DMake(43.8548338,-86.381856)];
//
//            break;
//        case 2:
//            [driverLocation.annotation setCoordinate:CLLocationCoordinate2DMake(43.8548338,-95.381856)];
//
//            break;
//        case 3:
//            [driverLocation.annotation setCoordinate:CLLocationCoordinate2DMake(43.8548338,-106.381856)];
//
//            break;
//        case 4:
//        default:
//
//            [driverLocation.annotation setCoordinate:CLLocationCoordinate2DMake(63.8548338,-106.381856)];
//
//            
//            break;
//    }
        [driverLocation.annotation setTitle:@"TAXI"];

    [driverLocation.annotation setCoordinate:CLLocationCoordinate2DMake([driverDictionary[@"lat"] floatValue], [driverDictionary[@"lng"] floatValue])];
    //FIXME: send driver loc to ui
    [summaryVC displayDriverLocation:driverLocation];
    }
    
}
-(void)socketError:(NSArray *)array
{

}
#pragma mark- Methods

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if ( (index >= 2)) {
        return nil;
    }
    if (index==0) {
        return summaryVC;
    }
    return detailsVC;
    // Create a new view controller and pass suitable data.
}
#pragma mark - Actions
- (IBAction)segementedChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        UIViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];

    }else
    {
        UIViewController *startingViewController = [self viewControllerAtIndex:1];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}
- (IBAction)dismissView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)callClicked:(id)sender {
}

@end
