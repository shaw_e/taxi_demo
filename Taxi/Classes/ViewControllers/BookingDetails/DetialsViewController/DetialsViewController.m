//
//  DetialsViewController.m
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "DetialsViewController.h"
#import "NSString+CheckValue.h"
#import "NSString+DateFormatting.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "BookingConnectionHandler.h"
#import "RoundCornerButton.h"
#import "UIViewController+Alert.h"



@interface DetialsViewController ()<UIActionSheetDelegate,ServicesConnectionDelegate>
{

    __weak IBOutlet UITextView *textView;
    
}
@end

@implementation DetialsViewController
{
    NSMutableString * txt;
    int fontSizeInPixel;
    BookingConnectionHandler * connectionHandler;
    __weak IBOutlet RoundCornerButton *actionButton;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [textView setAttributedText:[self getStringWithBooking:self.item]];
    [textView setHidden:YES];
    self.automaticallyAdjustsScrollViewInsets=NO;
    // Do any additional setup after loading the view.
}
- (IBAction)edit:(id)sender {
    // 1 = from. 2 = to, 3 = livery
    UIActionSheet * actionSheet=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Dismiss" destructiveButtonTitle:nil otherButtonTitles:@"Edit",@"Cancel", nil];
    [actionSheet showInView:self.view];
    

}
//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex ==0) {
//        int type = self.item.booking_details.details_tripTypeNumber.intValue;
//        if (type==1) {
//            EditLimoFromAirportViewController * edit=[[AppDelegate getAppDelegate].storyboard instantiateViewControllerWithIdentifier:@"EditLimoFromAirportViewController"];
//            [edit setOriginBooking:self.item];
//            [self.parentViewController presentViewController:edit animated:YES completion:nil];
//            
//            
//        }else if (type ==2)
//        {
//            EditLimoToAirportViewController * edit=[[AppDelegate getAppDelegate].storyboard instantiateViewControllerWithIdentifier:@"EditLimoToAirportViewController"];
//            [edit setOriginBooking:self.item];
//            [self.parentViewController presentViewController:edit animated:YES completion:nil];
//            
//        }else if (type ==3)
//        {
//            EditLiveryViewController * edit=[[AppDelegate getAppDelegate].storyboard instantiateViewControllerWithIdentifier:@"EditLiveryViewController"];
//            [edit setOriginBooking:self.item];
//            [self.parentViewController presentViewController:edit animated:YES completion:nil];
//            
//        }
// 
//    }else if (buttonIndex ==1)
//    {
//        [UIAlertView showWithTitle:@"" message:@"Are you sure that you want to delete the booking?" cancelButtonTitle:@"cancel" otherButtonTitles:@[@"delete"] tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
//            if (buttonIndex==1) {
//                if (!connectionDelegate) {
//                    connectionDelegate=[BookingController new];
//                    [connectionDelegate setDelegate:self];
//                }
//                [connectionDelegate cancelBookingWithID:self.item.booking_details.details_bookindID];
//                [actionButton setIsloading:YES];
//            }
//        }];
//        
//    }
//
//}
-(void)viewDidAppear:(BOOL)animated
{
    if (textView.isHidden) {
        [textView setContentOffset:CGPointZero animated:NO];
        [textView setHidden:NO];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setItem:(DTOBooking *)item
{
    _item=item;
    if (textView) {
        [textView setAttributedText:[self getStringWithBooking:self.item]];  
    }
}
- (NSAttributedString *)getStringWithBooking:(DTOBooking *)booking
{
    
    txt =[ NSMutableString new];
    //    NSString * fontSizeInPixel= @"14";
    fontSizeInPixel = 12;
    //
    [self startTable];
    [self addBoldKeyWithText:@"Booking#" boldValueWithText:booking.booking_details.details_confirmation_number];
//    //
//    if ([booking.booking_details.details_tripType hasValue]) {
//        [self addBoldKeyWithText:@"Service Type:" regualerValueWithText:booking.booking_details.details_tripType];
//    }
    [self addLine];
    //
    [self addBoldKeyWithText:@"Pickup Address (From):" regualerValueWithText:booking.booking_fromAddress.addressName];
    if ([booking.booking_fromAddress.addressFligtNumber hasValue]) {
        [self addBoldKeyWithText:@"AirLine:" regualerValueWithText:booking.booking_fromAddress.addressFligtNumber];
    }
    if ([booking.booking_fromAddress.addressTerminal hasValue]) {
        [self addBoldKeyWithText:@"Terminal:" regualerValueWithText:booking.booking_fromAddress.addressTerminal];
    }
    //Drop-off (To)
    [self addBoldKeyWithText:@"Drop-off (To):" regualerValueWithText:booking.booking_toAddress.addressName];
    if ([booking.booking_toAddress.addressFligtNumber hasValue]) {
        [self addBoldKeyWithText:@"AirLine:" regualerValueWithText:booking.booking_toAddress.addressFligtNumber];
    }
    if ([booking.booking_toAddress.addressTerminal hasValue]) {
        [self addBoldKeyWithText:@"Terminal:" regualerValueWithText:booking.booking_toAddress.addressTerminal];
    }
    [self addLine];
    //
    [self addBoldKeyWithText:@"Date:" regualerValueWithText:[booking.booking_details.details_date stringDateWithSourceFormat:sourceDateFormat  desiredFormat:desiredDateFormat]];
    [self addBoldKeyWithText:@"Time:" regualerValueWithText:[booking.booking_details.details_time stringDateWithSourceFormat:sourceTimeFormat  desiredFormat:desiredTimeFormat]];
    
    //Car Type:
    [self addBoldKeyWithText:@"Car Type:" regualerValueWithText:booking.booking_details.details_carTypeName];
//    //Passengers
//    [self addBoldKeyWithText:@"Passengers:" regualerValueWithText:booking.booking_details.details_passengerNumber];
//    //Bags
//    [self addBoldKeyWithText:@"Bags:" regualerValueWithText:booking.booking_details.details_bagsNumber];
//    //carseat
//    if (booking.booking_careseats.count>0) {
//        NSArray * array=[booking.booking_careseats valueForKeyPath:@"seat"];
//        NSString * string = [array componentsJoinedByString:@","];
//        [self addBoldKeyWithText:@"Child Seat:" regualerValueWithText:string];
//        
//    }
    //driver
    if ([booking.booking_driver.driver_fullName hasValue]) {
        NSString * name =booking.booking_driver.driver_fullName;
        [self addBoldKeyWithText:@"Driver:" regualerValueWithText:name];
        
    }
    [self addLine];
    
    [self endTable];
    //
    //
    ///
    [self addTitle:@"Cost" withFontSize:fontSizeInPixel+2];
    ///
    ///
    [self startSmallTable];
    //Rate:
    NSString * string = [[NSString alloc]initWithFormat:@"$%@",booking.booking_charges.charge_rate];
    [self addBoldKeyWithText:@"Rate" regualerValueWithText:string];
    //Echarges:
    string = [[NSString alloc]initWithFormat:@"$%@",booking.booking_charges.charge_extraCharges];
    [self addBoldKeyWithText:@"Echarges:" regualerValueWithText:string];
    //Seats:
    string = [[NSString alloc]initWithFormat:@"$%@",booking.booking_charges.charge_carSeats];
    [self addBoldKeyWithText:@"Car Seats:" regualerValueWithText:string];
    //Discount:
    string = [[NSString alloc]initWithFormat:@"%@%%",booking.booking_charges.charge_discount];
    [self addBoldKeyWithText:@"Discount:" regualerValueWithText:string];
    [self addLine];
    //Fare:
    string = [[NSString alloc]initWithFormat:@"$%@",booking.booking_charges.charge_total];
    [self addBoldKeyWithText:@"Fare:" regualerValueWithText:string];
    
    [self endTable];
    
    
    
    
    
    NSMutableAttributedString * attrTxt = [[NSMutableAttributedString alloc] initWithData:[txt dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    txt = nil;
    return attrTxt;
    
    
}
-(void)startTable
{
    [txt appendString:@" <div class=\"content-wrap\" style=\"\" ><table cellpadding=\"2\" cellspacing=\"8\" border=\"0\" width=\"100%\" style=\"font-family:Arial, Helvetica, sans-serif;margin-left:0px;\">"];
    
}
-(void)addBoldKeyWithText : (NSString *)key boldValueWithText:(NSString *)value
{
    [txt appendFormat:@"<tr style=\"font-size:%ipx;color:#333;\"><td style=\"text-align:left;\" colspan=\"2\" width=\"65%%\"><strong>%@</strong></td><td colspan=\"2\" style=\"text-align:left;\" width=\"35%%\"><strong>%@</strong></td></tr>",fontSizeInPixel,key,value];
    
}
-(void)addRegularKeyWithText : (NSString *)key regualerValueWithText:(NSString *)value
{
    [txt appendFormat:@"<tr style=\"font-size:%ipx;color:#333;\"><td style=\"text-align:left;\" colspan=\"2\" width=\"65%%\">%@</td><td colspan=\"2\" style=\"text-align:left;\" width=\"35%%\">%@</td></tr>",fontSizeInPixel,key,value];
    
}
-(void)addBoldKeyWithText : (NSString *)key regualerValueWithText:(NSString *)value
{
    [txt appendFormat:@"<tr style=\"font-size:%ipx;color:#333;\"><td style=\"text-align:left;vertical-align:top\" colspan=\"2\" width=\"65%%\"><strong>%@</strong></td><td colspan=\"2\" style=\"text-align:left;\" width=\"35%%\">%@</td></tr>",fontSizeInPixel,key,value];
    
}
-(void)addRegularKeyWithText : (NSString *)key BoldValueWithText:(NSString *)value
{
    [txt appendFormat:@"<tr style=\"font-size:%ipx;color:#333;\"><td style=\"text-align:left;\" colspan=\"2\" width=\"65%%\">%@</td><td colspan=\"2\" style=\"text-align:left;\" width=\"35%%\"><strong>%@</strong></td></tr>",fontSizeInPixel,key,value];
    
}
-(void)addLine
{
    [txt appendString:@"        <tr> <td colspan=\"4\" style=\"border-bottom:1px #aaa solid;\"></td></tr>"];
    
}
-(void)endTable
{
    [txt appendString:@"</table></div>"];
}
-(void)addTitle:(NSString *)text withFontSize:(int)size
{
    [txt appendFormat:@"<h3 style=\"font-size:%ipx;color:#333;\">%@</h3>",size,text];
}
-(void)startSmallTable
{
    [txt appendString:@" <div class=\"content-wrap\" style=\"\" ><table cellpadding=\"2\" cellspacing=\"8\" border=\"0\"  width=\"80%\" style=\"font-family:Arial, Helvetica, sans-serif;margin-left:5%%;margin-right:5%%;\">"];
    
}
-(void)dealloc
{

}
#pragma mark-BookingControllerDelegate
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    NSLog(@"%@",result);
    [actionButton setIsloading:NO];
    if ([result[@"responseCode"]isEqualToString:@"SUCCESS"]) {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }else
    {
        if ([[[result[@"errors"] objectForKey:@"msg"] objectAtIndex:0] hasValue]) {
            [self showAlertDialogWithMessage:[[result[@"errors"] objectForKey:@"msg"] objectAtIndex:0]];

        }else
        {
            [self showAlertDialogWithMessage:result[@"msg"]];

        }
    }
    
}
-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    [actionButton setIsloading:NO];
    [self showAlertDialogWithMessage:error.localizedDescription];
    
    
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    NSLog(@"%@",@"Server Down");
    [actionButton setIsloading:NO];
    [self showAlertDialogWithMessage:@"Server Down"];
    
    
}

/*
 
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
