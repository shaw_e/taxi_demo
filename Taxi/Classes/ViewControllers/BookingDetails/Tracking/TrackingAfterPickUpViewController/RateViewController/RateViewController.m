//
//  RateViewController.m
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "RateViewController.h"
#import "RateView.h"
#import "RoundCornerButton.h"
#import "NSString+CheckValue.h"
#import "UIViewController+Alert.h"
@interface RateViewController ()<RateViewDelegate,ServicesConnectionDelegate>
{

    __weak IBOutlet RateView *rateView;
    BookingConnectionHandler * connectionHandler;
    __weak IBOutlet RoundCornerButton *submitButton;
}

@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    rateView.notSelectedImage = [UIImage imageNamed:@"kermit_empty.png"];
    rateView.halfSelectedImage = [UIImage imageNamed:@"kermit_half.png"];
    rateView.fullSelectedImage = [UIImage imageNamed:@"kermit_full.png"];
    rateView.rating = 0;
    rateView.editable = YES;
    rateView.maxRating = 5;
    rateView.delegate = self;
    //
    [submitButton setLoadingColor:[UIColor blackColor]];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
//    self.statusLabel.text = [NSString stringWithFormat:@"Rating: %f", rating];
}
-(IBAction)submitClicked
{
    if (!connectionHandler) {
        connectionHandler =[BookingConnectionHandler new];
        [connectionHandler setDelegate:self];
    }
    [connectionHandler rateBookingWithID:self.booking.booking_details.details_bookindID withRate:rateView.rating];
    [submitButton setIsloading:YES];
}
#pragma mark-BookingControllerDelegate
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result
{
    NSLog(@"%@",result);
    [submitButton setIsloading:NO];
    if ([result[@"responseCode"]isEqualToString:@"SUCCESS"]) {

    }else
    {
        if ([[[result[@"errors"] objectForKey:@"msg"] objectAtIndex:0] hasValue]) {
            [self showAlertDialogWithMessage:[[result[@"errors"] objectForKey:@"msg"] objectAtIndex:0]];
\
        }else
        {
            [self showAlertDialogWithMessage:result[@"msg"]];
            
        }
    }
    
}
-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error
{
    [submitButton setIsloading:NO];
    [self showAlertDialogWithMessage:error.localizedDescription];
    
    
}
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag
{
    NSLog(@"%@",@"Server Down");
    [submitButton setIsloading:NO];
    [self showAlertDialogWithMessage:@"Server Down"];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
