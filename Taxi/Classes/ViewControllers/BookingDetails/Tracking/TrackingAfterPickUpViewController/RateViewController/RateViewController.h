//
//  RateViewController.h
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTOBooking.h"
#import "BookingConnectionHandler.h"

@interface RateViewController : UIViewController
@property(nonatomic,weak)DTOBooking * booking;
@end
