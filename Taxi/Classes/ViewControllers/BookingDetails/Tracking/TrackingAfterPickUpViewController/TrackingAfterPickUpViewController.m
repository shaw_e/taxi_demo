//
//  TrackingAfterPickUpViewController.m
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "TrackingAfterPickUpViewController.h"
#import "InfoViewController.h"
#import "RateViewController.h"


@interface TrackingAfterPickUpViewController ()<UIPageViewControllerDataSource>
{
    UIPageViewController * pageViewController;
    InfoViewController * infoVC;
    RateViewController * rateVC;
}
@end

@implementation TrackingAfterPickUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (!pageViewController) {
        //
        infoVC =[InfoViewController new];
        rateVC =[RateViewController new];
        [rateVC setBooking:self.booking];
        //
        pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        pageViewController.dataSource = self;
        
        UIViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        // Change the size of page view controller
        NSLog(@"%f",self.view.frame.size.height );
        NSLog(@"%f", mapView.frame.size.height+mapView.frame.origin.y);
        
        pageViewController.view.frame = CGRectMake(0, self.view.frame.size.height - 140, self.view.frame.size.width, 140);
//        [pageViewController.view setBackgroundColor:[UIColor grayColor]];
        [self addChildViewController:pageViewController];
        [self.view addSubview:pageViewController.view];
        [pageViewController didMoveToParentViewController:self];
        for (UIView *subview in pageViewController.view.subviews) {
            if ([subview isKindOfClass:[UIPageControl class]]) {
                UIPageControl *pageControl = (UIPageControl *)subview;
                pageControl.pageIndicatorTintColor = [UIColor grayColor];
                pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
//                pageControl.backgroundColor = [UIColor blueColor];
            }
        }

        
        
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if ( (index >= 2)) {
        return nil;
    }
    if (index ==1) {
        return infoVC;

    }else
    {
        return rateVC;

    }
    
    // Create a new view controller and pass suitable data.
}
- (int)indexOfViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[InfoViewController class]]) {
        return 1;
    }
    return 0;
}

#pragma mark - Page View Controller Data Source

//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
//{
//    NSUInteger index = [self indexOfViewController:viewController];
//    
//    if ((index == 0) || (index == NSNotFound)) {
//        return nil;
//    }
//    
//    index--;
//    return [self viewControllerAtIndex:index];
//}
//
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
//{
//    NSUInteger index = [self indexOfViewController:viewController];
//    
//    if (index == NSNotFound) {
//        return nil;
//    }
//    
//    index++;
//    if (index == 2) {
//        return nil;
//    }
//    return [self viewControllerAtIndex:index];
//}
//
//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 2;
//}

//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 0;
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
