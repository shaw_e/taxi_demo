//
//  TrackingParentViewController.h
//  Booking
//
//  Created by ahmed shawky on 3/20/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TrackingHeaderView.h"
#import "DTOBooking.h"
#import "DTOlocation.h"

@interface TrackingParentViewController : UIViewController
{
    __weak IBOutlet MKMapView *mapView;
    TrackingHeaderView * headerView;
    NSString * selectorToExecute;


}
@property (nonatomic,strong)DTOBooking * booking;
-(void)displayDriverLocation:(DTOlocation *)location;
-(void)updateDriverHeaderWithCarNumber:(NSString *)carPlat;
-(void)removeDriverLocation;
@end
