//
//  TrackingParentViewController.m
//  Booking
//
//  Created by ahmed shawky on 3/20/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "TrackingParentViewController.h"
#import "NSString+CheckValue.h"
#import "NSString+DateFormatting.h"
#import "Constants.h"
#import "CustomAnnotationView.h"
#import "NSString+CheckValue.h"

@interface TrackingParentViewController ()<MKMapViewDelegate>
{
    MKPointAnnotation * p1;
    MKPointAnnotation * p2;
    DTOlocation * driverLocation;
}

@end

@implementation TrackingParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    headerView =[[TrackingHeaderView alloc ]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, mapView.frame.origin.y)];
    [self.view addSubview:headerView];
    [mapView setDelegate:self];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [headerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, mapView.frame.origin.y)];

    
}
-(void)viewDidAppear:(BOOL)animated
{
    if (selectorToExecute.length>0) {
        SEL selector=NSSelectorFromString(selectorToExecute);
        if ([self respondsToSelector:selector]) {
            [self performSelectorOnMainThread:selector withObject:nil waitUntilDone:NO];
        }
        selectorToExecute=nil;
    }
    
}

-(void)setBooking:(DTOBooking *)booking
{
    _booking = booking;
    if (headerView) {
        [self loadUI];
    }else
    {
        selectorToExecute=NSStringFromSelector(@selector(loadUI));
    }
    
}
-(void)displayDriverLocation:(DTOlocation *)location
{
    if (driverLocation) {
        [mapView removeAnnotation:driverLocation.annotation];
    }
    driverLocation = location;
    [mapView addAnnotation:driverLocation.annotation];
    [self centerMap];

}
-(void)updateDriverHeaderWithCarNumber:(NSString *)carPlat
{
    if ([self.booking.booking_driver.driver_imagePath hasValue]) {
        [headerView.imgView setImageURLString:self.booking.booking_driver.driver_imagePath];
    }
    [headerView.carNumberLabel setText:carPlat];
    [headerView.driverNameLabel setText:self.booking.booking_driver.driver_fullName];
    
}
-(void)removeDriverLocation
{
    if (driverLocation) {
        [mapView removeAnnotation:driverLocation.annotation];
    }
}
-(void)loadUI
{

    [self createAndAddAnnotations];
//    if ([self.booking.booking_driver.d]) {
//        <#statements#>
//    }
    if ([self.booking.booking_driver.driver_fullName hasValue]) {
        [headerView.driverNameLabel setText: self.booking.booking_driver.driver_fullName];
    }
    if ([self.booking.booking_car.carCode hasValue]) {
        NSString * carCode =[[NSString alloc] initWithFormat:@"Car#%@",self.booking.booking_car.carCode];
        [headerView.carNumberLabel setText: carCode];

    }
    [headerView.etdLabel setText:@"ETA"];
    if ([self.booking.booking_details.details_time hasValue]) {
        [headerView.timeLabel setText:[self.booking.booking_details.details_time stringDateWithSourceFormat:sourceTimeFormat desiredFormat:desiredTimeFormat]];
    }

}
-(void)createAndAddAnnotations
{
    [mapView removeAnnotations:mapView.annotations];
    float pickUpLat=self.booking.booking_fromAddress.addressLat.floatValue;
    float pickUpLong=self.booking.booking_fromAddress.addressLong.floatValue;
    p1=[[MKPointAnnotation alloc]init];
    p1.coordinate=CLLocationCoordinate2DMake(pickUpLat, pickUpLong);
    p1.title=self.booking.booking_fromAddress.addressName;
    [mapView addAnnotation:p1];
    //
    float dropOffLat=self.booking.booking_toAddress.addressLat.floatValue;
    float dropOffLong=self.booking.booking_toAddress.addressLong.floatValue;
    p2=[[MKPointAnnotation alloc]init];
    p2.coordinate=CLLocationCoordinate2DMake(dropOffLat, dropOffLong);
    p2.title=self.booking.booking_toAddress.addressName;
    [mapView addAnnotation:p2];
    //
    [self centerMap];

}
-(void) centerMap
{
    [mapView showAnnotations:mapView.annotations animated:YES];
}
- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation)
        return nil;
    
    if (annotation == p1) {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pickUpAnn" color:[UIColor colorFromHexString:@"#a5c13e"]];
        annView.canShowCallout=YES;
        return annView;
        
    }else if (annotation == p2)
    {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"drop" color:[UIColor colorFromHexString:@"#c13e3e"]];
        
        return annView;
        
    }else //if (annotation == driverLocation.annotation)
    {
        CustomAnnotationView *annView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"drop" color:[UIColor colorFromHexString:@"#ffda44"]];
        
        return annView;

    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
