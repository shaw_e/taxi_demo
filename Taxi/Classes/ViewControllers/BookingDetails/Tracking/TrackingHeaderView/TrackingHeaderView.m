//
//  TrackingHeaderView.m
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "TrackingHeaderView.h"

@implementation TrackingHeaderView
{
    __weak UIView  * contentView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadViewAndOutletsWithFrame:frame];
    }
    return self;
}
-(void)loadViewAndOutletsWithFrame:(CGRect) frame
{
    UIView * view =[[NSBundle mainBundle]loadNibNamed:@"TrackingHeaderView" owner:self options:0][0];
    [view setTag:50];
    [view setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [self addSubview:view];
    contentView = view;

    //
    self.imgView=(UIImageView *)[view viewWithTag:100];
    self.driverNameLabel=(UILabel *)[view viewWithTag:200];
    self.carNumberLabel=(UILabel *)[view viewWithTag:300];
    self.etdLabel=(UILabel *)[view viewWithTag:400];
    self.timeLabel=(UILabel *)[view viewWithTag:500];
    //

}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [contentView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
