//
//  TrackingHeaderView.h
//  Booking
//
//  Created by ahmed shawky on 3/16/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface TrackingHeaderView : UIView
@property (nonatomic,weak) AsyncImageView * imgView;
@property (nonatomic,weak) UILabel * driverNameLabel;
@property (nonatomic,weak) UILabel * carNumberLabel;
@property (nonatomic,weak) UILabel * etdLabel;
@property (nonatomic,weak) UILabel * timeLabel;


@end
