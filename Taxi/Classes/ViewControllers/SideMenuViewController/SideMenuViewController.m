//
//  SideMenuViewController.m
//  Taxi
//
//  Created by ahmed shawky on 4/3/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "SideMenuViewController.h"

@interface SideMenuViewController ()
{
    __weak IBOutlet UITableView *tblView;

}
@end

@implementation SideMenuViewController
{
    NSArray * arrayTblV;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayTblV = @[@"Book Your Ride",@"My Rides",@"Payment"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- Table View
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return arrayTblV.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.textLabel setText:arrayTblV[indexPath.row]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        [self performSegueWithIdentifier:@"Main" sender:nil];
    }else
    {//Payment
        [self performSegueWithIdentifier:@"Payment" sender:nil];
    }
//    MyRide
//        [self performSegueWithIdentifier:@"MyRide" sender:nil];

}/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
