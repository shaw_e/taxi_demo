//
//  RoundCornerButton.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "RoundCornerButton.h"
#import "Animation.h"
@implementation RoundCornerButton
{
    
    __weak UIActivityIndicatorView *loadingSpinner;
    NSAttributedString *cashedString;
    
}
-(void)setIsloading:(BOOL)isLoading
{
    if (isLoading) {
        if (!loadingSpinner) {
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            spinner.frame = CGRectMake(self.frame.size.width/2 - spinner.frame.size.width/2, self.frame.size.height/2 - spinner.frame.size.height/2, spinner.frame.size.width, spinner.frame.size.height);
            [spinner startAnimating];
            if (self.loadingColor) {
                [spinner setColor:self.loadingColor];
            }
            
            [self addSubview:spinner];
            [self setAttributedTitle:nil forState:UIControlStateNormal];
            [self setTitle:@"" forState:UIControlStateNormal];
            [self setEnabled:NO];
            //
            loadingSpinner=spinner;

        }
        cashedString = self.titleLabel.attributedText;
        
        
    }else
    {
        [loadingSpinner removeFromSuperview];
        [self setAttributedTitle:cashedString forState:UIControlStateNormal];
        [self setEnabled:YES];
        cashedString=nil;
        
    }
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [Animation roundCornerForView:self withAngle:7];
    }
    return self;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}


@end
