//
//  RoundCornerButton.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundCornerButton : UIButton
@property (nonatomic,strong)UIColor * loadingColor;
-(void)setIsloading:(BOOL)isLoading;

@end
