//
//  MyRideCell.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRideCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pickUpLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOff;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
