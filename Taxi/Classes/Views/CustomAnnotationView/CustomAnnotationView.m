//
//  CustomAnnotationView.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "CustomAnnotationView.h"

@interface CustomAnnotationView ()
@property (nonatomic) CGFloat width;
@end

@implementation CustomAnnotationView
{
    UIColor * selectedColor;
}

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier color:(UIColor *)color
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:8];
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        self.label = label;
        selectedColor =color;
        [self adjustLabelWidth:annotation];
        
        self.opaque = false;
        self.centerOffset = CGPointMake(0, -11);
    }
    return self;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    [super setAnnotation:annotation];
    [self adjustLabelWidth:annotation];
}

- (void)adjustLabelWidth:(id<MKAnnotation>)annotation {
    NSString *title;
    
    if ([annotation respondsToSelector:@selector(title)] && self.label) {
        title = [annotation title];
        NSDictionary *attributes = @{ NSFontAttributeName : self.label.font };
        CGSize size = [title sizeWithAttributes:attributes];
        self.width = MAX(size.width + 6, 22);
    } else {
        self.width = 22;
    }
    
    self.label.frame = CGRectMake(0, 33, self.width, 10);
    self.label.text = title;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.width, 44);
}

- (void)drawRect:(CGRect)rect {
    CGFloat lineWidth = 1.0;
    CGFloat radius = 10 + lineWidth / 2.0;
    CGFloat bubbleHeight = 20;
    CGPoint point = CGPointMake(self.width / 2.0, 31);
    CGPoint nextPoint;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:point];
    
    nextPoint = CGPointMake(point.x - radius, point.y - bubbleHeight);
    [path addCurveToPoint:nextPoint controlPoint1:CGPointMake(point.x, point.y - bubbleHeight / 2.0) controlPoint2:CGPointMake(nextPoint.x, nextPoint.y + bubbleHeight / 2.0)];
    
    CGPoint center = CGPointMake(point.x, nextPoint.y);
    [path addArcWithCenter:center radius:radius startAngle:M_PI endAngle:0 clockwise:TRUE];
    
    point = CGPointMake(nextPoint.x + radius * 2.0, nextPoint.y);
    nextPoint = CGPointMake(point.x - radius, point.y + bubbleHeight);
    [path addCurveToPoint:nextPoint controlPoint1:CGPointMake(point.x, point.y + bubbleHeight / 2.0) controlPoint2:CGPointMake(nextPoint.x, nextPoint.y - bubbleHeight / 2.0)];
    
    [[UIColor blackColor] setStroke];
    //    [[UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:0.8] setFill];
        [selectedColor setFill];
    path.lineWidth = lineWidth;
    
    [path fill];
    [path stroke];
    
    CGRect bubbleRect = CGRectMake(1, 33, self.width - 2.0, 10);
    path = [UIBezierPath bezierPathWithRoundedRect:bubbleRect cornerRadius:5];
    path.lineWidth = lineWidth;
    [path fill];
    [path stroke];
    
    path = [UIBezierPath bezierPathWithArcCenter:center radius:radius / 3.0 startAngle:0 endAngle:M_PI * 2.0 clockwise:TRUE];
    path.lineWidth = lineWidth;
    [[UIColor whiteColor] setFill];
    [path fill];
}
@end