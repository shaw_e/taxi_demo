//
//  PaymentHandler.m
//  Taxi
//
//  Created by Ahmed shawky on 4/7/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "PaymentHandler.h"

@implementation PaymentHandler
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.arrayOfCards=[aDecoder decodeObjectForKey:@"arrayOfCards"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.arrayOfCards forKey:@"arrayOfCards"];

}
+(instancetype)shared
{
    static PaymentHandler * shared= nil;
    static dispatch_once_t onceToken1;
    dispatch_once(&onceToken1, ^{
        NSData * data =[[NSUserDefaults standardUserDefaults] objectForKey:@"PaymentHandler"];
        shared=[NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (!shared) {
            shared =[PaymentHandler new];
            shared.arrayOfCards=[NSMutableArray new];
        }
    });
    return shared;
}
-(void)save
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"PaymentHandler"];

}

@end
