//
//  PaymentHandler.h
//  Taxi
//
//  Created by Ahmed shawky on 4/7/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentHandler : NSObject<NSCoding>
@property (nonatomic,strong)NSMutableArray * arrayOfCards;
+(instancetype)shared;
-(void)save;

@end
