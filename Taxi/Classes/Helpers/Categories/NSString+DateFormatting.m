//
//  NSString+DateFormatting.m
//  Fujirah
//
//  Created by Ahmed shawky on 11/12/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import "NSString+DateFormatting.h"
//#import "Language.h"

@implementation NSString (DateFormatting)
-(NSDate *)dateValue
{
    //create date format
    // date from string :self
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    
    NSMutableString * dateFormattingString=[NSMutableString stringWithString:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    if (self.length>19) {
        for (int x=21; x<self.length+2; x++) {
            if (x==21) {
                [dateFormattingString appendString:@"."];
                
            }else
            {
                [dateFormattingString appendString:@"S"];
                
            }
        }
    }
    [dateFormatter setDateFormat:dateFormattingString];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate * date=[dateFormatter dateFromString:self];
    //this will set date's time components to 00:00
    return date;
}
-(NSDate *)dateValueFromConvertedDate
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy,EEEE"];
//    if(CURRENT_LANGUAGE==LanguageEnglish)
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//    else
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar_EG"]];
//
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate * date=[dateFormatter dateFromString:self];

    //this will set date's time components to 00:00
    return date;

}
-(NSDate *)dateValueShortFormat
{
    //create date format
    // date from string :self
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate * date=[dateFormatter dateFromString:self];
    //this will set date's time components to 00:00
    return date;
}
-(NSDate *)dateValueForTicket
{

    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];

    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mma"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];

    
    NSDate * date=[dateFormatter dateFromString:self];
    //this will set date's time components to 00:00
    return date;

}
- (NSString *)stringDateWithSourceFormat:(NSString *)sourceFormat desiredFormat:(NSString *)desiredFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *orignalDate   =  [dateFormatter dateFromString:self];
    
    [dateFormatter setDateFormat:desiredFormat];
    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    return finalString;
}

@end
