//
//  UIView+Coordinates.h
//  Kotobna
//
//  Created by ShawE on 2/20/15.
//  Copyright (c) 2015 Shaw_e. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Coordinates)
-(void)setHeight:(float)h;
-(void)setWidth:(float)w;
-(void)setX:(float)x;
-(void)setY:(float)y;
-(float)height;
-(float)width;
-(float)x;
-(float)y;
@end
