//
//  UIView+Coordinates.m
//  Kotobna
//
//  Created by ShawE on 2/20/15.
//  Copyright (c) 2015 Shaw_e. All rights reserved.
//

#import "UIView+Coordinates.h"

@implementation UIView (Coordinates)
-(void)setHeight:(float)h
{
    CGRect frame=self.frame;
    frame.size.height=h;
    [self setFrame:frame];
    
}
-(void)setWidth:(float)w
{
    CGRect frame=self.frame;
    frame.size.width=w;
    [self setFrame:frame];

}
-(void)setX:(float)x
{
    CGRect frame=self.frame;
    frame.origin.x=x;
    [self setFrame:frame];

}
-(void)setY:(float)y
{
    CGRect frame=self.frame;
    frame.origin.y=y;
    [self setFrame:frame];
}
-(float)height
{
    return self.frame.size.height;
}
-(float)width
{
    return self.frame.size.width;
}
-(float)x
{
    return self.frame.origin.x;
}
-(float)y
{
    return self.frame.origin.y;
}
@end
