//
//  UIViewController+Alert.h
//  Fujirah
//
//  Created by ahmed shawky on 2/3/16.
//  Copyright © 2016 700apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)
-(void)showAlertDialogWithMessage:(NSString *)message;

@end
