//
//  UIViewController+NavigationBar.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "UIViewController+NavigationBar.h"
#import "SWRevealViewController.h"
@implementation UIViewController (NavigationBar)
-(void)customizeBack
{

        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 25, 25)];
        [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    [backButton.imageView setContentMode:UIViewContentModeScaleAspectFit];

        [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton setTintColor:[UIColor blackColor]];
        self.navigationItem.leftBarButtonItem = backButtonItem;

}
-(void)createMenu
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 25, 25)];
    [btn addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [btn.imageView setContentMode:UIViewContentModeScaleAspectFit];

//    UIBarButtonItem *barButton =
//    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
  //  [[UIBarButtonItem alloc]initWithTitle:@"M" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [barButton setTintColor:[UIColor blackColor]];

    [self.navigationItem setRightBarButtonItem:barButton];

}
-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)openMenu
{
    [self.navigationController.revealViewController rightRevealToggleAnimated:YES];
}


@end
