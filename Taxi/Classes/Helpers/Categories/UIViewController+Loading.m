//
//  UIViewController+Loading.m
//  Fujirah
//
//  Created by Ahmed shawky on 11/12/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import "UIViewController+Loading.h"

@implementation UIViewController (Loading)
-(void)startLoading
{
    [ZAActivityBar showWithStatus:@"Loading" forAction:@"ac"];
    [self.view setUserInteractionEnabled:NO];
    [self.view setAlpha:0.5];
}
-(void)endLoading
{
    [ZAActivityBar showSuccessWithStatus:@"Done" forAction:@"ac"];
    [self.view setUserInteractionEnabled:YES];
    [self.view setAlpha:1.0];

}
-(void)endLoadingWithSuccessMessage:(NSString *)message
{
    [ZAActivityBar showSuccessWithStatus:message forAction:@"ac"];
    [self.view setUserInteractionEnabled:YES];
    [self.view setAlpha:1.0];

}
-(void)dismissLoading
{
    [ZAActivityBar dismissForAction:@"ac"];
    [self.view setUserInteractionEnabled:YES];
    [self.view setAlpha:1.0];
}
-(void)endLoadingWithErrorMessage:(NSString *)message
{
    [ZAActivityBar showErrorWithStatus:message forAction:@"ac"];
    [self.view setUserInteractionEnabled:YES];
    [self.view setAlpha:1.0];

}
@end
