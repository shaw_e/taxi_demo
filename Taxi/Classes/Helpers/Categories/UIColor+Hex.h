//
//  UIColor+Hex.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/15/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#define HEX_DARK_GRAY @"#747474"
#define HEX_LIGHT_GRAY @"#c8c8c8"
#define HEX_DARK_BLUE @"#034ea2"
#define HEX_LIGHT_BLUE @"#008ecc"
#define HEX_MID_GRAY @"#9b9b9b"
@interface UIColor (Hex)
+ (UIColor *)colorFromHexString:(NSString *)hexString ;
+ (UIColor *)colorFromHexString:(NSString *)hexString  alpha:(float)alpha;
@end
