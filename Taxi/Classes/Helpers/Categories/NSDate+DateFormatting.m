//
//  NSDate+DateFormatting.m
//  Fujirah
//
//  Created by Ahmed shawky on 11/24/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import "NSDate+DateFormatting.h"
@implementation NSDate (DateFormatting)
-(NSString *)stringValue
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    [dateFormatter setTimeStyle:NSDateFormatterFullStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

//    if(CURRENT_LANGUAGE==LanguageEnglish)
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//    else
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar_EG"]];
    
    
    
    return [dateFormatter stringFromDate:self];
}
-(NSString *)stringValueWithHours
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
//    if(CURRENT_LANGUAGE==LanguageEnglish)
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//    else
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar_EG"]];
    
    
    
    return [dateFormatter stringFromDate:self];
}

-(NSString *)smallStringValue
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];

//    if(CURRENT_LANGUAGE==LanguageEnglish)
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//    else
//        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar_EG"]];
    
    return [dateFormatter stringFromDate:self];
}
-(NSString*)stringValueForServices
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];

    return [dateFormatter stringFromDate:self];
}
-(NSString*)desiredDateStringValue
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:desiredDateFormat];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)desiredTimeStringValue
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:desiredTimeFormat];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    return [dateFormatter stringFromDate:self];
}

@end
