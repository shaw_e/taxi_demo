//
//  UIViewController+Alert.m
//  Fujirah
//
//  Created by ahmed shawky on 2/3/16.
//  Copyright © 2016 700apps. All rights reserved.
//

#import "UIViewController+Alert.h"
#import "Constants.h"

@implementation UIViewController (Alert)
-(void)showAlertDialogWithMessage:(NSString *)message
{
    if(object_getClassName(@"UIAlertController")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:message
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }

}
@end
