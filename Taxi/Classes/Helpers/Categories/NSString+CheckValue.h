//
//  NSString+CheckValue.h
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CheckValue)
-(BOOL)hasValue;
@end
