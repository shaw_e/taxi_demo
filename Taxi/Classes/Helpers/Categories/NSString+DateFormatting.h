//
//  NSString+DateFormatting.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/12/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateFormatting)
-(NSDate *)dateValue;
-(NSDate *)dateValueShortFormat;
-(NSDate *)dateValueFromConvertedDate;
-(NSDate *)dateValueForTicket;
- (NSString *)stringDateWithSourceFormat:(NSString *)sourceFormat desiredFormat:(NSString *)desiredFormat;
@end
