//
//  NSDate+DateFormatting.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/24/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface NSDate (DateFormatting)
-(NSString *)stringValue;
-(NSString *)stringValueWithHours;
-(NSString *)smallStringValue;
-(NSString*)stringValueForServices;
-(NSString*)desiredDateStringValue;
-(NSString *)desiredTimeStringValue;

@end
