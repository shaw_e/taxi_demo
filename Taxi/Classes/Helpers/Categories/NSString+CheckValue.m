//
//  NSString+CheckValue.m
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "NSString+CheckValue.h"

@implementation NSString (CheckValue)
-(BOOL)hasValue
{
    if ([self respondsToSelector:@selector(length)]) {
        return (self.length>0);

    }
    return NO;

}
@end
