//
//  UIViewController+Loading.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/12/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZAActivityBar.h"
@interface UIViewController (Loading)
-(void)startLoading;
-(void)endLoading;
-(void)endLoadingWithSuccessMessage:(NSString *)message;
-(void)dismissLoading;
-(void)endLoadingWithErrorMessage:(NSString *)message;
@end
