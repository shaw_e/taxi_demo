//
//  UIViewController+NavigationBar.h
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NavigationBar)
-(void)customizeBack;
-(void)createMenu;
@end
