//
//  CarConnectionHandler.h
//  Taxi
//
//  Created by Ahmed shawky on 4/6/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "ServicesConnectionHandler.h"
#import "Car.h"
@interface CarConnectionHandler : ServicesConnectionHandler
@property (nonatomic,strong) NSMutableArray * arrayOfCars;
@property (nonatomic) int selectedCar ;
-(void)retrieveCars;
+(CarConnectionHandler *)sharedHandler;
-(Car *)currentSelectCar;
@end
