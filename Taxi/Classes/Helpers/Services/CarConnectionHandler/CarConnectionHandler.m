//
//  CarConnectionHandler.m
//  Taxi
//
//  Created by Ahmed shawky on 4/6/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "CarConnectionHandler.h"

@implementation CarConnectionHandler
+(CarConnectionHandler *)sharedHandler
{
    static CarConnectionHandler * shared= nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [CarConnectionHandler new];
    });
    return shared;
}
-(void)retrieveCars
{
    urlString =[NSString stringWithFormat:SERVICES_PREFIX,@"guests/mob/getList/CarTypes"];
    executeSelector = @selector(executeGetServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];
}
-(void)finish
{
    [super finish];
    self.delegate = nil;
}
-(Car *)currentSelectCar
{
    if (self.arrayOfCars.count>0) {
        return self.arrayOfCars[self.selectedCar];
    }
    return nil;
}
@end
