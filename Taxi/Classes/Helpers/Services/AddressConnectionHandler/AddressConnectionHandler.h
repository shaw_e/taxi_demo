//
//  AddressConnectionHandler.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "ServicesConnectionHandler.h"
#import "Address.h"
@interface AddressConnectionHandler : ServicesConnectionHandler
-(void)retrieveAddress;
-(void)addAddress:(Address *)item;
@end
