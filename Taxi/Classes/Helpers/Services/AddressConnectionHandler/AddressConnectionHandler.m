//
//  AddressConnectionHandler.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "AddressConnectionHandler.h"
@implementation AddressConnectionHandler
-(void)retrieveAddress
{
    NSDictionary * dic =@{@"token":TOKEN};
    postData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    urlString = [[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/getAddresses"];

    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];
  
}
-(void)addAddress:(Address *)item
{
    NSDictionary * dic =@{@"address":item.addressName,@"lat":item.addressLat,@"lng":item.addressLong,@"apt":@"",@"buzz_code":@"",@"major_intersection":@"",@"token":TOKEN};
    postData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    urlString = [[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/AddAddress"];
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];

}
@end
