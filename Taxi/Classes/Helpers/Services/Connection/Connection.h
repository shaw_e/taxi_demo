//
//  Connection.h
//  MapWithRoute
//
//  Created by Ahmed shawky on 10/29/15.
//  Copyright © 2015 Ahmed shawky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

typedef enum{
    RTInternetError= 1,
    RTStatusCodeError=2,
    RTUsalError=3,
    RTSuccess=4
} ResultType;

@interface ConnectionResult : NSObject
@property (strong,nonatomic)NSData * receivedData;
@property (nonatomic)int statusCode;
@property (nonatomic)ResultType type;
@property (nonatomic,strong)NSError * error;

@end

typedef  void (^FinishBlock)(ConnectionResult * result);


@interface Connection : NSObject
+(NSURLSessionDataTask *)getDataFromUrlString:(NSString *)stringOfURL withResultBlock:(FinishBlock)blk;
+(NSURLSessionDataTask *)postData:(NSData*)dataToPost toUrlString:(NSString *)stringOfURL withResultBlock:(FinishBlock)blk;
@end
