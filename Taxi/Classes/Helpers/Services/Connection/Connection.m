//
//  Connection.m
//  MapWithRoute
//
//  Created by Ahmed shawky on 10/29/15.
//  Copyright © 2015 Ahmed shawky. All rights reserved.
//

#import "Connection.h"
#define TIME_OUT 30
@implementation Connection
+(NSURLSessionDataTask *)getDataFromUrlString:(NSString *)stringOfURL withResultBlock:(FinishBlock)blk
{
    //internetCheck
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    
    if (netStatus == NotReachable)
    {
        ConnectionResult * result=[ConnectionResult new];
        result.type=RTInternetError;
        //send the result in UI Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            blk(result);
        });
        return nil;
    }else{
        //create the NSURLSessionConfiguration
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        //create the request
        NSURL * url=[NSURL URLWithString:stringOfURL];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setTimeoutInterval:TIME_OUT];
        //create the NSURLSessionUploadTask
        NSURLSessionDataTask *task =[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            //dto to send the result
            ConnectionResult * result=[ConnectionResult new];
            //retrieve status code
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int code = (int)[httpResponse statusCode];
            if (code>499&&code<601) {
                result.type=RTStatusCodeError;
                result.statusCode=code;
                blk(result);
            }else if (error) {
                result.type=RTUsalError;
                result.error=error;
                blk(result);
            }else{
                result.type=RTSuccess;
                result.receivedData=data;
            }
            //send the result in UI Thread
            dispatch_async(dispatch_get_main_queue(), ^{
                blk(result);
            });
        }];
        
        [task resume];
        return task;
    }
    
}
+(NSURLSessionDataTask *)postData:(NSData *)dataToPost toUrlString:(NSString *)stringOfURL withResultBlock:(FinishBlock)blk
{
    //internetCheck
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    
    if (netStatus == NotReachable)
    {
        ConnectionResult * result=[ConnectionResult new];
        result.type=RTInternetError;
        //send the result in UI Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            blk(result);
        });
        return nil;
    }else{
        //create the NSURLSessionConfiguration
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        //create the request
        NSURL * url=[NSURL URLWithString:stringOfURL];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:dataToPost];
        [request setTimeoutInterval:TIME_OUT];

        //create the NSURLSessionUploadTask
        NSURLSessionDataTask *task =[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            //dto to send the result
            ConnectionResult * result=[ConnectionResult new];
            //retrieve status code
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int code = (int)[httpResponse statusCode];
            if (code>499&&code<601) {
                result.type=RTStatusCodeError;
                result.statusCode=code;
                blk(result);
            }else if (error) {
                result.type=RTUsalError;
                result.error=error;
                blk(result);
            }else{
                result.type=RTSuccess;
                result.receivedData=data;
            }
            //send the result in UI Thread
            dispatch_async(dispatch_get_main_queue(), ^{
                blk(result);
            });
        }];
        
        [task resume];
        return task;
    }
}
@end



@implementation ConnectionResult
@end