//
//  UserConnectionHandler.m
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "UserConnectionHandler.h"

@implementation UserConnectionHandler
-(void)registerFromDictionary:(NSDictionary *)dictionary
{
    urlString=[[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/Register"];
    postData=[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];
}
-(void)logInFromDixtionary:(NSDictionary *)dictionary
{
    urlString=[[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/Login"];
    postData=[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];

}
@end
