//
//  UserConnectionHandler.h
//  Taxi
//
//  Created by Ahmed shawky on 4/5/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "ServicesConnectionHandler.h"

@interface UserConnectionHandler : ServicesConnectionHandler
-(void)registerFromDictionary:(NSDictionary *)dictionary;
-(void)logInFromDixtionary:(NSDictionary *)dictionart;
@end
