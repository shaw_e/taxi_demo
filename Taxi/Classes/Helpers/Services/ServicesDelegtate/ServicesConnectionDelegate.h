//
//  ServicesConnectionDelegate.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/11/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServicesConnectionDelegate <NSObject>
-(void)servicesWithTag:(NSString *)tag didEndWith:(id)result;
-(void)servicesDidFailDueToInternetErrorWithTag:(NSString *)tag ;
-(void)servicesWithTag:(NSString *)tag DidFailDueToError:(NSError *)error;
-(void)servicesDidFailDueToServerDownWithTag:(NSString *)tag ;
@end
