//
//  BookingConnectionHandler.h
//  Taxi
//
//  Created by Ahmed shawky on 4/6/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "ServicesConnectionHandler.h"

@interface BookingConnectionHandler : ServicesConnectionHandler
-(void)costFromDictionary:(NSDictionary *)params;
-(void)bookRideFromDictionary:(NSDictionary *)params;
-(void)rateBookingWithID:(NSString*)booking_id withRate:(float)rate;

@end
