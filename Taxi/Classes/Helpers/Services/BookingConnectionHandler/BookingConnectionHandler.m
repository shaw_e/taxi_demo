//
//  BookingConnectionHandler.m
//  Taxi
//
//  Created by Ahmed shawky on 4/6/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "BookingConnectionHandler.h"

@implementation BookingConnectionHandler
-(void)costFromDictionary:(NSDictionary *)params
{
//    if (!params) {
//        return;
//    }
    NSDictionary * dic =params;
    postData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    urlString = [[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/getBookingRate"];
    
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];

}
-(void)bookRideFromDictionary:(NSDictionary *)params
{
    NSDictionary * dic =params;
    postData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    urlString = [[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/createBooking"];
    
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];

}
-(void)rateBookingWithID:(NSString *)booking_id withRate:(float)rate
{
//getBookingRate
    NSDictionary * postDic=@{@"booking_id":booking_id,@"rate":@(rate),@"token":TOKEN};

    NSDictionary * dic =postDic;
    postData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    urlString = [[NSString alloc] initWithFormat:SERVICES_PREFIX,@"customers/mob/addRating"];
    
    executeSelector=@selector(executePostServices);
    parseSelector = @selector(parseJsonResult:);
    [self start];

}
@end
