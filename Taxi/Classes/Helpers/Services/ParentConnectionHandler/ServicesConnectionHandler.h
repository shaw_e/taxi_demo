//
//  ServicesConnectionHandler.h
//  Fujirah
//
//  Created by Ahmed shawky on 11/11/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesConnectionDelegate.h"
#import "Connection.h"
#import "Constants.h"
@interface ServicesConnectionHandler : NSObject
{
    SEL executeSelector;
    SEL parseSelector;
    NSData * postData;
    NSString * urlString;
    NSURLSessionDataTask * task;
}
@property (weak)id<ServicesConnectionDelegate>delegate;
@property(nonatomic,strong)NSString *  tagString;
- (instancetype)initWithTag:(NSString*)_tag;
//
-(void)start;
-(void)cancel;
-(void)finish;
//
-(void)executePostServices;
-(void)executeGetServices;
//
-(void)parseStringResult:(ConnectionResult *)result;
-(void)parseJsonResult:(ConnectionResult *)result;
//
-(BOOL)hasNoDataFromResultDictionary:(NSDictionary*)dictionary;
//date
-(void)retieveDate;
@end
