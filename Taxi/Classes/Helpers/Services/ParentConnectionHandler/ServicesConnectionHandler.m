//
//  ServicesConnectionHandler.m
//  Fujirah
//
//  Created by Ahmed shawky on 11/11/15.
//  Copyright © 2015 700apps. All rights reserved.
//

#import "ServicesConnectionHandler.h"

@implementation ServicesConnectionHandler
#pragma mark- Life Cycle
-(instancetype)initWithTag:(NSString *)_tag
{
    self = [super init];
    if (self) {
        self.tagString=_tag;
    }
    return self;
}

-(void)dealloc
{
    [self finish];
}
#pragma mark start&finish
-(void)start
{
    [self performSelectorOnMainThread:executeSelector withObject:nil waitUntilDone:YES];
}
-(void)cancel
{
    [task cancel];
}
-(void)finish
{
    postData=nil;
    urlString=nil;
    self.tagString=nil;
    executeSelector=nil;
    parseSelector=nil;
    task=nil;
}
#pragma mark Connecting
-(void)executePostServices
{
    task=[Connection postData:postData toUrlString:urlString withResultBlock:^(ConnectionResult *result) {
        if (parseSelector) {
            [self performSelectorOnMainThread:parseSelector withObject:result waitUntilDone:YES];
        }
    }];
}
-(void)executeGetServices
{
    task=[Connection getDataFromUrlString:urlString withResultBlock:^(ConnectionResult *result) {
        if (parseSelector) {
            [self performSelectorOnMainThread:parseSelector withObject:result waitUntilDone:YES];
        }
    }];
}
#pragma mark Parsing
-(void)parseStringResult:(ConnectionResult *)result
{
    if (result.type==RTInternetError) {
        [self.delegate servicesDidFailDueToInternetErrorWithTag:self.tagString];
    }else if(result.type==RTStatusCodeError)
    {
        [self.delegate servicesDidFailDueToServerDownWithTag:self.tagString];
    }else if (result.type==RTUsalError){
        [self.delegate servicesWithTag:self.tagString DidFailDueToError:result.error];
        
    }else if (result.type==RTSuccess)
    {
        NSString * resultString=[[NSString alloc]initWithData:result.receivedData encoding:NSUTF8StringEncoding];
        [self.delegate servicesWithTag:self.tagString didEndWith:resultString];
    }
    
}
-(void)parseJsonResult:(ConnectionResult *)result
{
    if (result.type==RTInternetError) {
        [self.delegate servicesDidFailDueToInternetErrorWithTag:self.tagString];
    }else if(result.type==RTStatusCodeError)
    {
        [self.delegate servicesDidFailDueToServerDownWithTag:self.tagString];
    }else if (result.type==RTUsalError){
        if (result.error.code==-1003) {
            [self.delegate servicesDidFailDueToInternetErrorWithTag:self.tagString];

        }else
        {
            [self.delegate servicesWithTag:self.tagString DidFailDueToError:result.error];

        }
        
    }else if (result.type==RTSuccess)
    {
        id json = [NSJSONSerialization JSONObjectWithData:result.receivedData options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate servicesWithTag:self.tagString didEndWith:json];
    }
    
}
-(void)retieveDate
{
    NSString * suffix=@"Tender/GetServerTime";
    urlString=[NSString stringWithFormat:SERVICES_PREFIX,suffix];
    executeSelector=@selector(executeGetServices);
    parseSelector=@selector(parseJsonResult:);
    [self start];

}
#pragma mark Helpers
-(BOOL)hasNoDataFromResultDictionary:(NSDictionary*)dictionary
{
    NSDictionary * resultStatusDic=[dictionary[@"data"] objectAtIndex:0];
    return [resultStatusDic[@"ResultData"]intValue]==0;
}
@end
