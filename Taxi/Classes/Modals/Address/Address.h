//
//  Address.h
//  Booking
//
//  Created by Amir F.Ramadan on 8/14/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject

@property (strong, nonatomic) NSString *addressName, *addressApt, *addresBuzzCode, *addressCustomerID, *addressID, *addressLat, *addressLong, *addressMajorIntersections, *addressTitle,*addressFligtNumber,*addressTerminal,*airportID;

- (void)populateAddressWithDic:(NSDictionary *)address;

@end
