//
//  Address.m
//  Booking
//
//  Created by Amir F.Ramadan on 8/14/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import "Address.h"

@implementation Address
@synthesize addressName, addressApt, addresBuzzCode, addressCustomerID, addressID, addressLat, addressLong, addressMajorIntersections, addressTitle,addressFligtNumber,addressTerminal,airportID;

- (void)populateAddressWithDic:(NSDictionary *)address
{
    
    addressName = [address objectForKey:@"address"];
    addressApt = [address objectForKey:@"apt"];
    addresBuzzCode = [address objectForKey:@"buzz_code"];
    addressCustomerID = [address objectForKey:@"customer_id"];
    addressID = [address objectForKey:@"id"];
    addressLat = [address objectForKey:@"lat"];
    addressLong = [address objectForKey:@"lng"];
    addressMajorIntersections = [address objectForKey:@"major_intersection"];
    addressFligtNumber= [address objectForKey:@"flight_number"];
    addressTerminal = [address objectForKey:@"terminal"];
    if (address[@"airport_id"]) {
        airportID=[address objectForKey:@"airport_id"];
    }
}

@end
