//
//  DTOPayment.m
//  Taxi
//
//  Created by Ahmed shawky on 4/7/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "DTOPayment.h"

@implementation DTOPayment
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.card=[aDecoder decodeObjectForKey:@"card"];
        self.name=[aDecoder decodeObjectForKey:@"name"];
        self.ccv=[aDecoder decodeObjectForKey:@"ccv"];
        self.zipCode=[aDecoder decodeObjectForKey:@"zipCode"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.card forKey:@"card"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.ccv forKey:@"ccv"];
    [aCoder encodeObject:self.card forKey:@"zipCode"];
}
@end
