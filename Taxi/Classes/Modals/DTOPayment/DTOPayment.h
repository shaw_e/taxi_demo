//
//  DTOPayment.h
//  Taxi
//
//  Created by Ahmed shawky on 4/7/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTOPayment : NSObject<NSCoding>
@property (nonatomic,strong) NSString * card;
@property (nonatomic,strong) NSString * zipCode;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * ccv;
@end
