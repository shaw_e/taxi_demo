//
//  Constants.h
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//
#import "UIColor+Hex.h"
#import "Customer.h"

#define SERVICES_PREFIX @"http://dev.rytalo.ca/system/public/remote/%@"

#define THEME_COLOR     [UIColor colorFromHexString:@"#ffda44"]

#define TOKEN [[Customer currentCustomer] token]



#define sourceDateFormat                    @"yyyy-MM-dd"
#define sourceTimeFormat                    @"HH:mm:ss"
#define desiredDateFormat                   @"EEE MMM dd"
#define desiredTimeFormat                   @"HH:mm"



#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

