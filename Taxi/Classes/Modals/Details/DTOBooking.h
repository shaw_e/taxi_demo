//
//  DTOBooking.h
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTOCharges.h"
#import "DTOBookingDetails.h"
#import "Address.h"
//#import "Carseat.h"
#import "DTODriver.h"
#import "Customer.h"
#import "Car.h"



@interface DTOBooking : NSObject


@property (nonatomic,strong) DTOBookingDetails * booking_details;
@property (nonatomic,strong) DTOCharges * booking_charges;
@property (nonatomic,strong) Address * booking_fromAddress;
@property (nonatomic,strong) Address * booking_toAddress;
@property (nonatomic,strong) DTODriver * booking_driver;
@property (nonatomic,strong) Customer * booking_customer;
@property (nonatomic,strong) Car * booking_car;
@property (strong, nonatomic) NSMutableArray *booking_careseats;


- (instancetype)initWithDictionary:(NSDictionary *)dic;
-(void)refreshPropetiesFromDictionary:(NSDictionary *)dic;
@end
