//
//  DTOBooking.m
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "DTOBooking.h"

@implementation DTOBooking
- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        [self refreshPropetiesFromDictionary:dic];
        
    }
    return self;
}
-(void)refreshPropetiesFromDictionary:(NSDictionary *)dic
{
    self.booking_details = [[DTOBookingDetails alloc]initWithDictionary:dic[@"details"]];
    self.booking_charges = [[DTOCharges alloc]initWithDictionary:dic[@"charges"]];
    //
    self.booking_fromAddress =[Address new];
    [self.booking_fromAddress populateAddressWithDic:dic[@"fromLocation"]];
    //
    self.booking_toAddress =[Address new];
    [self.booking_toAddress populateAddressWithDic:dic[@"toLocation"]];
    //
//    self.booking_careseats=[NSMutableArray new];
//    for (NSDictionary * dictionary in dic[@"carseats"]) {
//        Carseat * item=[Carseat new];
//        [item initCarseatWithDic:dictionary];
//        [self.booking_careseats addObject:item];
//    }
    //
    self.booking_driver = [[DTODriver alloc]initWithDictionary:dic[@"driver"]];
    //
    self.booking_customer=[Customer new];
    [self.booking_customer populateCustomerWithDictionary:dic[@"customer"]];
    //
    if ([dic objectForKey:@"carDetails"]) {
        self.booking_car =[[Car alloc]initReservedCarFromDic:dic[@"carDetails"]];
    }
    

}
@end
