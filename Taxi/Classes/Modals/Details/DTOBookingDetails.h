//
//  DTOBookingDetails.h
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTOBookingDetails : NSObject

//number_of_bags  number_of_passengers   duration  distance

@property(nonatomic,strong) NSString * details_time;
@property(nonatomic,strong) NSString * details_date;
@property(nonatomic,strong) NSString * details_status;
@property(nonatomic,strong) NSString * details_rideStatus;
@property(nonatomic,strong) NSString * details_carType;
@property(nonatomic,strong) NSString * details_carTypeName;
@property(nonatomic,strong) NSString * details_confirmation_number;
@property(nonatomic,strong) NSString * details_tripType;
@property(nonatomic,strong) NSString * details_tripTypeNumber;
@property(nonatomic,strong) NSString * details_bookingType;

@property(nonatomic,strong) NSString * details_bagsNumber;
@property(nonatomic,strong) NSString * details_passengerNumber;
@property(nonatomic,strong) NSString * details_duration;
@property(nonatomic,strong) NSString * details_distance;
@property (nonatomic,strong)NSString * details_bookindID;
@property (nonatomic,strong)NSString * details_notes;

- (instancetype)initWithDictionary:(NSDictionary *)dic;
@end
