//
//  DTOCharges.h
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTOCharges : NSObject
@property (strong, nonatomic) NSString *charge_rate, *charge_extraCharges, *charge_discount,*charge_discountAmount, *charge_carSeats, *charge_total;
- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
