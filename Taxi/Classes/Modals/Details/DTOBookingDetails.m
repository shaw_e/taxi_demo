//
//  DTOBookingDetails.m
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "DTOBookingDetails.h"

@implementation DTOBookingDetails
- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.details_bookingType= dic[@"booking_type"];
        self.details_carType= dic[@"car_type"];
        self.details_carTypeName= dic[@"car_type_name"];
        self.details_confirmation_number= dic[@"confirmation_no"];
        self.details_date= dic[@"date"];
        self.details_rideStatus= dic[@"ridestatus"];
        self.details_status= dic[@"status"];
        self.details_time= dic[@"time"];
        self.details_tripType= dic[@"trip_type"];
        self.details_tripTypeNumber=dic[@"booking_trip_type"];
        self.details_passengerNumber = dic[@"number_of_passengers"];
        self.details_distance= dic[@"distance"];
        self.details_duration = dic[@"duration"];
        self.details_bagsNumber= dic[@"number_of_bags"];
        self.details_bookindID = dic [@"id"];
        self.details_notes =dic[@"notes"];
    }
    return self;
}
@end
