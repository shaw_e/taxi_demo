//
//  DTOCharges.m
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "DTOCharges.h"

@implementation DTOCharges
- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
//        rate = [[rateDic objectForKey:@"details"] objectForKey:@"rate"];
//        rateExtraCharges = [[rateDic objectForKey:@"details"] objectForKey:@"extra_charges"];
//        rateDiscount = [[rateDic objectForKey:@"details"] objectForKey:@"discount"];
//        rateCarseats = [[rateDic objectForKey:@"details"] objectForKey:@"car_seat"];
//        rateTotal = [rateDic objectForKey:@"rates"];
        
        self.charge_discount=dic[@"discount"];
        self.charge_rate=dic[@"rates"];
        self.charge_carSeats=dic[@"car_seat"];
        self.charge_discountAmount=dic[@"discount_amount"];
        self.charge_total=dic[@"totals"];
        self.charge_extraCharges=dic[@"extra_charges"];
        

    }
    return self;
}
@end
