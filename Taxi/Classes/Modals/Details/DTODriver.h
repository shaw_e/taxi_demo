//
//  DTODriver.h
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTODriver : NSObject
@property (nonatomic,strong) NSString * driver_fullName;
@property (nonatomic,strong) NSString * driver_firstName;
@property (nonatomic,strong) NSString * driver_lastName;
//phone
@property (nonatomic,strong) NSString * driver_phone;
@property (nonatomic,strong) NSString * driver_imagePath;
- (instancetype)initWithDictionary:(NSDictionary *)dic;
@end
