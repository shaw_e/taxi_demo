//
//  DTODriver.m
//  Booking
//
//  Created by ahmed shawky on 3/14/16.
//  Copyright © 2016 rytalo. All rights reserved.
//

#import "DTODriver.h"
#import "NSString+CheckValue.h"

@implementation DTODriver
- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.driver_firstName=dic[@"first_name"];
        self.driver_lastName=dic[@"last_name"];
        NSMutableString * fullName = [NSMutableString new];
        if ([self.driver_firstName hasValue]) {
            [fullName appendFormat:@"%@ ",self.driver_firstName];
        }
        if ([self.driver_lastName hasValue]) {
            [fullName appendFormat:@"%@",self.driver_lastName];
        }
        self.driver_fullName = fullName;
        self.driver_phone=dic[@"phone"];
        if ([dic[@"photo"] respondsToSelector:@selector(length)]) {
            self.driver_imagePath=dic[@"photo"];
        }
    }
    return self;

}
@end
