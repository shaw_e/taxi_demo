//
//  Car.h
//  Booking
//
//  Created by Amir F.Ramadan on 8/13/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject

@property (strong, nonatomic) NSString *carID, *carType, *carImage ,* carCode;
@property (nonatomic) BOOL isChecked;

- (void)initCarWithDic:(NSDictionary *)car;
- (instancetype)initReservedCarFromDic:(NSDictionary *)car;
@end
