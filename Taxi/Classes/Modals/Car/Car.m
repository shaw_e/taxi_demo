//
//  Car.m
//  Booking
//
//  Created by Amir F.Ramadan on 8/13/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import "Car.h"

@implementation Car
@synthesize carID, carImage, carType,carCode;
@synthesize isChecked;


- (void)initCarWithDic:(NSDictionary *)car
{
    carID = [car objectForKey:@"id"];
    carImage = [car objectForKey:@"photo"];
    carType = [car objectForKey:@"name"];
    
    isChecked = NO;
}
- (instancetype)initReservedCarFromDic:(NSDictionary *)car
{
    self = [super init];
    if (self) {
        carID = [car objectForKey:@"id"];
        carCode = [car objectForKey:@"code"];
        carType = [car objectForKey:@"name"];
 
    }
    return self;
}

@end
