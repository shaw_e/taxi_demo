//
//  DTOlocation.m
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import "DTOlocation.h"

@implementation DTOlocation
- (id)copyWithZone:(NSZone *)zone
{
    DTOlocation * copy = [[[self class] alloc] init];
    if (copy) {
        copy.address = [self.address copy];
        copy.annotation=self.annotation;
    }
    
    return copy;
    // Copying code here.
}

@end
