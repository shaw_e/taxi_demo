//
//  DTOlocation.h
//  Taxi
//
//  Created by Ahmed shawky on 4/4/16.
//  Copyright © 2016 ahmed shawky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface DTOlocation : NSObject<NSCopying>
@property (nonatomic,strong) NSString * address;

@property (nonatomic,strong) MKPointAnnotation * annotation;
@end
