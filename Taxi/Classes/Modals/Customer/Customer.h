//
//  Customer.h
//  Booking
//
//  Created by Amir F.Ramadan on 8/13/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject

@property (strong, nonatomic) NSString *customerFirstName, *customerLastName, *customerEmail,
*customerPhone, *customerphoto, *token, *companyID, *customerPassword,
*customerAddress, *customerID;

- (void)populateCustomerWithDictionary:(NSDictionary *) customerDic;
+ (Customer *)currentCustomer;
- (void)setAsCurrent;

@end
