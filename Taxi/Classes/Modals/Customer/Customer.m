//
//  Customer.m
//  Booking
//
//  Created by Amir F.Ramadan on 8/13/15.
//  Copyright (c) 2015 rytalo. All rights reserved.
//

#import "Customer.h"
static Customer * currentCashed ;

@implementation Customer
@synthesize customerFirstName, customerLastName, customerEmail,
customerPhone, customerphoto, token, companyID, customerPassword,
customerAddress, customerID;

- (void)populateCustomerWithDictionary:(NSDictionary *)customerDic
{
    if ([customerDic objectForKey:@"first_name"])
        customerFirstName = [customerDic objectForKey:@"first_name"];
    if ([customerDic objectForKey:@"password"])
        customerPassword = [customerDic objectForKey:@"password"];
    if ([customerDic objectForKey:@"last_name"])
        customerLastName = [customerDic objectForKey:@"last_name"];
    if ([customerDic objectForKey:@"email"])
        customerEmail = [customerDic objectForKey:@"email"];
    if ([customerDic objectForKey:@"phone"])
        customerPhone = [customerDic objectForKey:@"phone"];
    if ([customerDic objectForKey:@"id"])
        customerID = [customerDic objectForKey:@"id"];
    if ([customerDic objectForKey:@"photo"])
        customerphoto = [customerDic objectForKey:@"photo"];
    if ([customerDic objectForKey:@"company_id"])
        companyID = [customerDic objectForKey:@"company_id"];
    if ([customerDic objectForKey:@"remember_token"])
        token = [customerDic objectForKey:@"remember_token"];
}
+(Customer *)currentCustomer
{
    return currentCashed;
}
-(void)setAsCurrent
{
    currentCashed =self;
}

@end
